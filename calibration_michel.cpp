
//#include "Math/GSLMinimizer.h"
#include "Minuit2/Minuit2Minimizer.h"
#include "Math/Functor.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TH1F.h"

#include<iostream>




Double32_t square(const Double32_t *weights)
{
TFile* f_input = new TFile("cosmics_sum.root");
TTree* events = (TTree*)(f_input->Get("events"));

float val1;
float val2;
float val3;
float val4;
float val5;
float val6;
Double_t square_i;
Double_t square_sum=0;
Double_t energy_i;
Double_t energy_sum;
int nevents = events->GetEntries();
float time;
float energynocalib;
Double_t rec_energy;
events->SetBranchStatus("*",0);
events->SetBranchStatus("gamma_ch1",1);
events->SetBranchStatus("gamma_ch2",1);
events->SetBranchStatus("gamma_ch3",1);
events->SetBranchStatus("gamma_ch4",1);
events->SetBranchStatus("gamma_ch5",1);
events->SetBranchStatus("gamma_ch6",1);
events->SetBranchStatus("gamma_sum_nocalib",1);
events->SetBranchStatus("time_gamma_sum_nocalib",1);
events->SetBranchAddress("gamma_ch1",&val1);
events->SetBranchAddress("gamma_ch2",&val2);
events->SetBranchAddress("gamma_ch3",&val3);
events->SetBranchAddress("gamma_ch4",&val4);
events->SetBranchAddress("gamma_ch5",&val5);
events->SetBranchAddress("gamma_ch6",&val6);
events->SetBranchAddress("gamma_sum_nocalib",&energynocalib);
events->SetBranchAddress("time_gamma_sum_nocalib",&time);

const Double32_t Wi1 = weights[0];
const Double32_t Wi2 = weights[1];
const Double32_t Wi3 = weights[2];
const Double32_t Wi4 = weights[3];
const Double32_t Wi5 = weights[4];
const Double32_t Wi6 = weights[5];

Double32_t sum = Wi1+Wi2+Wi3+Wi4+Wi5+Wi6;
Double32_t w1,w2,w3,w4,w5,w6;
w1 = Wi1*6/sum;
w2 = Wi2*6/sum;
w3 = Wi3*6/sum;
w4 = Wi4*6/sum;
w5 = Wi5*6/sum;
w6 = Wi6*6/sum;


for(int i=0; i<nevents; i++)
    {
    events->GetEntry(i);
    rec_energy= (val1+val2+val3+val4+val5+val6)/energynocalib;
    if(0.8<rec_energy<1.2)
        {
        energy_i=(w1*val1+w2*val2+w3*val3+w4*val4+w5*val5+w6*val6);
        energy_sum+=energy_i;
        square_i=energy_i*energy_i;
        square_sum+=square_i;
        }
    else{continue;}
    }


Double32_t relsquare = square_sum/energy_sum;
std::cout << w1 <<"   " << w2 << "  " << w3 << "    " << w4 << "    " << w5 << "    " << w6 << std::endl;
std::cout << relsquare << std::endl;
f_input->Close();
return -relsquare;
}

int calibration_michel()
{
TFile* f_inputinit = new TFile("./../rootNew/michel/michel_1245to1247.root");
TFile* f_output = new TFile("./../AnalysisRoot/calibration_michel.root","RECREATE");
TTree* initevents = (TTree*)(f_inputinit->Get("events"));
int nevents = initevents->GetEntries();
TCanvas* canvas = new TCanvas("energy_distribution","energy_distribution");
canvas->Divide(2,1);

//draw initial energy distribution
canvas->cd(1);
Double32_t initialenergy;
TH1D* initialenergydist = new TH1D("initial_energy_distribution","before calibration",100,0,200000);
float val1;
float val2;
float val3;
float val4;
float val5;
float val6;
float inittime;
float initsumnocalib;
initevents->SetBranchAddress("gamma_ch1",&val1);
initevents->SetBranchAddress("gamma_ch2",&val2);
initevents->SetBranchAddress("gamma_ch3",&val3);
initevents->SetBranchAddress("gamma_ch4",&val4);
initevents->SetBranchAddress("gamma_ch5",&val5);
initevents->SetBranchAddress("gamma_ch6",&val6);
initevents->SetBranchAddress("gamma_sum_nocalib",&initsumnocalib);

initevents->SetBranchAddress("time_gamma_sum_nocalib",&inittime);
for(int i=0; i<nevents; i++)
    {
    initevents->GetEntry(i);
    initialenergy= val1+val2+val3+val4+val5+val6;
    if(initialenergy/initsumnocalib>0.8 && initialenergy/initsumnocalib<1.2)
        {
        initialenergydist->Fill(initialenergy);
        }
    else{continue;}


    }
initialenergydist->Draw();
f_output->WriteTObject(initialenergydist);
f_inputinit->Close();


//do the minimzation
Double32_t initweights[6]={1,1,1,1,1,1};
Double32_t steps[6]= {0.5,0.5,0.5,0.5,0.5,0.5};

ROOT::Math::GSLMinimizer min( ROOT::Math::kVectorBFGS );
//ROOT::Minuit2::Minuit2Minimizer min ( ROOT::Minuit2::kMigrad );
min.SetMaxFunctionCalls(100000000);
min.SetMaxIterations(10000000);
min.SetTolerance(0.00000001);

ROOT::Math::Functor f(&square,6);


min.SetFunction(f);


min.SetVariable(0,"Wi1",initweights[0], steps[0]);
min.SetVariable(1,"Wi2",initweights[1], steps[1]);
min.SetVariable(2,"Wi3",initweights[2], steps[2]);
min.SetVariable(3,"Wi4",initweights[3], steps[3]);
min.SetVariable(4,"Wi5",initweights[4], steps[4]);
min.SetVariable(5,"Wi6",initweights[5], steps[5]);

min.SetVariableLimits(0,0.5,2.);
min.SetVariableLimits(1,0.5,2.);
min.SetVariableLimits(2,0.5,2.);
min.SetVariableLimits(3,0.5,2.);
min.SetVariableLimits(4,0.5,2.);
min.SetVariableLimits(5,0.5,2.);



min.Minimize();
std::cout<< "test" << std::endl;
const Double32_t *finalW = min.X();
Double32_t finalweights[6];
Double32_t finalnorm = finalW[0]+finalW[1]+finalW[2]+finalW[3]+finalW[4]+finalW[5];
finalweights[0]= finalW[0]*6/finalnorm;
finalweights[1]= finalW[1]*6/finalnorm;
finalweights[2]= finalW[2]*6/finalnorm;
finalweights[3]= finalW[3]*6/finalnorm;
finalweights[4]= finalW[4]*6/finalnorm;
finalweights[5]= finalW[5]*6/finalnorm;
std::cout << "Minimum chisquare: " << square(finalweights) << std::endl;
std::cout << "w1=" << finalweights[0] << std::endl;
std::cout << "w2=" << finalweights[1] << std::endl;
std::cout << "w3=" << finalweights[2] << std::endl;
std::cout << "w4=" << finalweights[3] << std::endl;
std::cout << "w5=" << finalweights[4] << std::endl;
std::cout << "w6=" << finalweights[5] << std::endl;

//draw final energy distribution
TFile* f_inputfinal = new TFile("cosmics_sum.root");
f_output->cd();
canvas->cd(2);
Double32_t finalenergy;
TH1D* finalenergydist = new TH1D("final_energy_distribution","after calibration",100,0,200000);

float finaltime;
float energynocalib;
TTree* finalevents = (TTree*)(f_inputfinal->Get("events"));
finalevents->SetBranchAddress("gamma_ch1",&val1);
finalevents->SetBranchAddress("gamma_ch2",&val2);
finalevents->SetBranchAddress("gamma_ch3",&val3);
finalevents->SetBranchAddress("gamma_ch4",&val4);
finalevents->SetBranchAddress("gamma_ch5",&val5);
finalevents->SetBranchAddress("gamma_ch6",&val6);
finalevents->SetBranchAddress("time_gamma_sum_nocalib",&finaltime);
finalevents->SetBranchAddress("gamma_sum_nocalib",&energynocalib);


std::ostringstream finalsumstream;
finalsumstream << "(" << finalweights[0] << " * gamma_ch1 + " << finalweights[1] << " * gamma_ch2 + " << finalweights[2] << " * gamma_ch3 + " << finalweights[3] << " * gamma_ch4 + " << finalweights[4] << " * gamma_ch5 + " << finalweights[5] << " * gamma_ch6)";
const char* finalsumchar = finalsumstream.str().c_str();

std::ostringstream finalcutstream;
finalcutstream << finalsumstream.str() << "/gamma_sum_nocalib>0.8 && " << finalsumstream.str() << "/gamma_sum_nocalib < 1.2";
const char* finalcutchar = finalcutstream.str().c_str();

std::ostringstream finaldrawcutstream;
finaldrawcutstream << finalsumstream.str() << ">>final_energy_distribution";
const char* finaldrawcutchar = finaldrawcutstream.str().c_str();

finalevents->Draw(finaldrawcutchar,finalcutchar);

finalenergydist->Draw();

f_output->WriteTObject(finalenergydist);
f_output->WriteTObject(canvas);
f_inputfinal->Close();





TTree* t_weights = new TTree("weights", "weights");
Double32_t tmp;
t_weights->Branch("b_weights", &tmp);
for (int j = 0; j<6; j++)
    {
    tmp = finalweights[j];
    }
f_output->Write();


f_output->Close();
   return 0;

}


