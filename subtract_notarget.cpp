#include <iostream>
#include <sstream>

#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFractionFitter.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TH1.h"
#include "THStack.h"
#include "TLegend.h"
#include "TVirtualFitter.h"
#include "TCut.h"

void subtract_notarget()
{
    //define fit range
    Double_t range_low  = 150000;
    Double_t range_high = 220000;

    //define properties of histograms
    Int_t hist_min=0;
    Int_t hist_max=220000;
    Int_t hist_nbins=100;

    //get Files with data
    TFile* f_signal = new TFile("./../rootNew/gamma_pi-stop/sum_gamma_pi-stop.root");

    TFile* f_notarget = new TFile("./../rootNew/no_target_gamma_pi-stop/no_target_gamma_pi-stop_sum.root");

    //create output file
    TFile* f_out = new TFile ("./../rootNew/background_subtracted/sum_gamma_pi-stop_minus_notarget.root","RECREATE");

    //define cuts and objects to draw
    Double_t w1=1.13576;                      //weights for channels
    Double_t w2=1.034;
    Double_t w3=1.12747;
    Double_t w4=1.03648;
    Double_t w5=0.632663;
    Double_t w6=1.03362;
    Double_t rec_upper = 1.2;           //check if channels were reconstructed correctly
    Double_t rec_lower = 0.8;
    Double_t time_lower = 142;          //time cut
    Double_t time_upper = 146;
    Double_t time_ele_sci1_low=136;     //time cut on electron signal
    Double_t time_ele_sci1_up=150;
    Double_t time_ele_sci2_low=136;
    Double_t time_ele_sci2_up=150;
    Double_t coincidencewindow = 5;

    //define selection what to draw
    std::ostringstream selstream;
    selstream << w1 << "*gamma_ch1+" << w2 << "*gamma_ch2+" << w3 << "*gamma_ch3+" << w4 << "*gamma_ch4+" << w5 << "*gamma_ch5+" << w6 <<"*gamma_ch6";
    const char* sel = selstream.str().c_str();

    std::ostringstream rec_cutstream;
    rec_cutstream << "("<< selstream.str() << ")/gamma_sum_nocalib > " << rec_lower << " && (" << selstream.str() << ")/gamma_sum_nocalib < " << rec_upper;
    TCut rec_cut = rec_cutstream.str().c_str();

    std::ostringstream time_cutstream;
    time_cutstream << "time_gamma_sum_nocalib >=" << time_lower << " && time_gamma_sum_nocalib <=" << time_upper;
    TCut time_cut = time_cutstream.str().c_str();

    std::ostringstream ele_sci1cutstream;
    ele_sci1cutstream << "time_ele_sci1>=" << time_ele_sci1_low << " && time_ele_sci1<=" << time_ele_sci1_up;
    TCut ele_sci1cut = ele_sci1cutstream.str().c_str();

    std::ostringstream ele_sci2cutstream;
    ele_sci2cutstream << "time_ele_sci2>=" << time_ele_sci2_low << " && time_ele_sci2<=" << time_ele_sci2_up;
    TCut ele_sci2cut = ele_sci2cutstream.str().c_str();

    std::ostringstream ele_coincidence_cutstream;
    ele_coincidence_cutstream << "TMath::Abs(time_ele_sci2-time_ele_sci1)<=" << coincidencewindow;
    TCut coincidence_cut = ele_coincidence_cutstream.str().c_str();

    TCut cut = rec_cut && time_cut && ele_sci1cut && ele_sci2cut && coincidence_cut;

    //Set trees
    TTree* t_signal = (TTree*)f_signal->Get("events");
    t_signal->SetBranchStatus("*",0);
    t_signal->SetBranchStatus("gamma_ch1",1);
    t_signal->SetBranchStatus("gamma_ch2",1);
    t_signal->SetBranchStatus("gamma_ch3",1);
    t_signal->SetBranchStatus("gamma_ch4",1);
    t_signal->SetBranchStatus("gamma_ch5",1);
    t_signal->SetBranchStatus("gamma_ch6",1);
    t_signal->SetBranchStatus("gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_ele_sci1",1);
    t_signal->SetBranchStatus("time_ele_sci2",1);


    TTree* t_notarget = (TTree*)f_notarget->Get("events");
    t_notarget->SetBranchStatus("*",0);
    t_notarget->SetBranchStatus("gamma_ch1",1);
    t_notarget->SetBranchStatus("gamma_ch2",1);
    t_notarget->SetBranchStatus("gamma_ch3",1);
    t_notarget->SetBranchStatus("gamma_ch4",1);
    t_notarget->SetBranchStatus("gamma_ch5",1);
    t_notarget->SetBranchStatus("gamma_ch6",1);
    t_notarget->SetBranchStatus("gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_ele_sci1",1);
    t_notarget->SetBranchStatus("time_ele_sci2",1);

    //create histograms without cuts
    TH1F* h_signal = new TH1F("h_signal","h_signal",hist_nbins,hist_min,hist_max);
    TH1F* h_notarget = new TH1F("h_notarget","h_notarget",hist_nbins,hist_min,hist_max);

    //create histograms with cuts
    TH1F* h_cut_signal = new TH1F("h_cut_signal","h_signal with cuts",hist_nbins,hist_min,hist_max);
    TH1F* h_cut_notarget = new TH1F("h_cut_notarget","h_notarget with cuts",hist_nbins,hist_min,hist_max);

    //get histograms
    std::ostringstream get_signal_stream;
    get_signal_stream << sel << ">> h_signal";
    const char* get_signal = get_signal_stream.str().c_str();

    std::ostringstream get_notarget_stream;
    get_notarget_stream << sel << ">> h_notarget";
    const char* get_notarget = get_notarget_stream.str().c_str();

    std::ostringstream get_cut_signal_stream;
    get_cut_signal_stream << sel << ">> h_cut_signal";
    const char* get_cut_signal = get_cut_signal_stream.str().c_str();

    std::ostringstream get_cut_notarget_stream;
    get_cut_notarget_stream << sel << ">> h_cut_notarget";
    const char* get_cut_notarget = get_cut_notarget_stream.str().c_str();

    t_signal->Draw(get_signal,rec_cut,"goff");
    t_notarget->Draw(get_notarget,rec_cut,"goff");
    t_signal->Draw(get_cut_signal,cut,"goff");
    t_notarget->Draw(get_cut_notarget,cut,"goff");


    Int_t bin_low = h_signal->FindBin(range_low);
    Int_t bin_high = h_signal->FindBin(range_high);




    //get scale factors
    Double_t norm_signal = h_signal->Integral(bin_low,bin_high);
    Double_t norm_notarget = h_notarget->Integral(bin_low,bin_high);


    Double_t scale_notarget = norm_signal/norm_notarget;

    std::cout << "scale notarget:" << scale_notarget << std::endl;
    std::cout << "norm_signal: " << norm_signal << "    norm_notarget: " << norm_notarget<< std::endl;

    //subtract background histograms
    TH1F* h_clean_signal = (TH1F*)h_cut_signal->Clone("h_clean_signal");
    h_clean_signal->Add(h_cut_notarget,-scale_notarget);

    //draw output
    f_out->cd();
    h_notarget->Scale(scale_notarget);
    TCanvas* c_out = new TCanvas("results","results");
    c_out->Divide(3,1);
    c_out->cd(1);
    gPad->SetLogy();
	
	h_notarget->SetTitle("H_notarget without cuts");
    h_notarget->SetFillColor(kRed);

    TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
    leg->AddEntry(h_signal,"taken data","l,e");
    leg->AddEntry(h_notarget,"background without target","f");
    h_notarget->Draw();
    h_signal->DrawClone("ep,SAME");
    leg->Draw("same");

    c_out->cd(2);
    h_cut_notarget->SetFillColor(kRed);
    h_cut_notarget->Scale(scale_notarget);
    h_cut_notarget->Draw();
    h_cut_signal->Draw("same,ep");
    c_out->Update();
    c_out->cd(3);
    h_clean_signal->Draw();

    //save output
    f_out->WriteTObject(c_out);
    f_out->WriteTObject(h_clean_signal);
    f_out->WriteTObject(h_signal);
    f_out->WriteTObject(h_notarget);
    f_out->WriteTObject(h_cut_signal);
    f_out->WriteTObject(h_cut_notarget);


    //close files and canvas
    c_out->Close();
    f_signal->Close();
    f_notarget->Close();
    f_out->Close();
}
