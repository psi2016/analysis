#include "TFile.h"
#include "TString.h"
#include <iostream>
#include "TTree.h"
#include "TH1.h"
#include <sstream>


void Rescale()
{
// Variables Declaration
	
	string file1, file2, leaf1, leaf2;
	Double_t factor; 						//the factor, you want to scale the x-axis with
	Double_t offset;						//offset - shifts all entries to the left or right
	float oldvalue; 						//here you safe the event entry;
	Double_t newvalue;
	std::ostringstream hfile1stream;
	std::ostringstream ofile1stream;
	std::ostringstream draw1stream;
	std::ostringstream rescaledleaf1stream;
	std::ostringstream file1pathstream;
	

// Input
/*
	std:cout << "Rescales and/or shifts a histogram like a*x + b " << std::endl;
	std:cout << "---------------------------------" << std::endl;
	std::cout << "File name (for example: run_1000.root) " << std::endl;
	std::cin >> file1;
	std::cout << "Leaf name (for example: gamma_ch1) "<< std::endl;
	std::cin >> leaf1;
	std::cout << "Rescale Factor a" << std::endl;
	std::cin >> factor;
	std::cout << "Offset b" << std::endl;
	std::cin >> offset;
*/
	

// SAMPLE FOR FAST DEBUGGING (Comment Input part and uncomment this part to set it up.)

    file1 = "run_990.root";
	leaf1 = "gamma_ch1";
	factor = 0.1;
	offset = 5000;

	
// Setup Strings

	file1pathstream << "./../rootNew/" << file1; 		// File Path
	const char *file1path = file1pathstream.str().c_str();

	const char* leaf1name = leaf1.c_str();
	const char* file1name = file1.c_str();

    cout << "file1name: " << file1path << " and leaf1name: " << leaf1name << endl;
	
	hfile1stream << "h_" << leaf1;
	const char* hfile1 = hfile1stream.str().c_str(); 	// hfile1 = "h_gamma_ch1" for example
	
	ofile1stream << "o_" << leaf1;
	const char* ofile1 = ofile1stream.str().c_str(); 	// hfile1 = "o_gamma_ch1" for example
	
	draw1stream << leaf1 << ">>" << hfile1;
	const char* draw1char = draw1stream.str().c_str(); 	// draw1char = "gamma_ch1>>h_gamma_ch1" for example

	rescaledleaf1stream << "Rescaled " << leaf1;
	const char* rescaledleaf1char = rescaledleaf1stream.str().c_str();
	
	
// Open/Create files and read tree

    TFile* if1 = new TFile(file1path, file1name);

    bool f = if1->IsOpen();
    if (f) {cout << file1path << " is open" << endl;}
    else {cout << "NOT OPEN" << endl; return;}

	TFile* of = new TFile("Rescale.root", "recreate");
	TTree* tree1 = (TTree*)if1->Get("events");
	
// Find Histogram Range

    //Double_t max = tree1->GetMaximum("gamma_ch1");
    Double_t max = tree1->GetMaximum(leaf1name);

// Copy initial histogram

	TH1F* hist_old = new TH1F(hfile1,leaf1name,2000,0,max); 
    tree1->Draw(draw1char,"","goff");		// Draws the initial histogram, and doesn't open the canvas window.
	
// Set Status and Address
	
	tree1->SetBranchStatus("*",0); 			//deactivates all not needed branches/leafs. makes the code run much faster
	tree1->SetBranchStatus(leaf1name,1); 	//activates leaf "gamma_ch1" (for example), so that you can read it
	tree1->SetBranchAddress(leaf1name,&oldvalue); 	//tells the code, that you will later onwards safe the event value in "oldvalue"

// Get number of entries

	int nentries = tree1->GetEntries();
	
// Create new rescaled histograms

	max = max*factor + offset;	//the numbers have to be big enough for the new histogram
	TH1F* hist = new TH1F(ofile1,rescaledleaf1char,2000,0,max); 	
	
// Fill New Rescaled Histogram
	
	for(int j = 0; j < nentries; j++){
		tree1->GetEntry(j); 					// safes the values of the activated branches 
		newvalue = oldvalue*factor + offset;	// into their address, here for example "gamma_ch1" into oldvalue
		hist->Fill(newvalue); 					// fills the new histogram
	}

// Save & Write

	of->WriteTObject(hist_old);
	of->WriteTObject(hist);
	
	
// Close or Delete

	if1->Close();
	of->Close();
	
	if1->Delete();
	of->Delete();
	
// Print "Done"

	printf("%s\n","Done. See file Rescale.root");
}
