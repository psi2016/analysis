//program producing many histograms to find suiting cuts on times, coincidences etc

#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TF2.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TObject.h"
#include "TCut.h"

void find_cuts(const char* infile)
{
    TFile* f_in = new TFile(infile);
    TTree* t_events = (TTree*)f_in->Get("events");
    TFile* f_out = new TFile("./../AnalysisRoot/cutfinding.root","RECREATE");
    TCut cut_ele1 = "time_ele_sci1 >= 136 && time_ele_sci1 <= 150";
    TCut cut_ele2 = "time_ele_sci2 >= 136 && time_ele_sci2 <= 150";
    TCut rec_cut = "(gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib >=0.8 && (gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib <= 1.2";
    TCut cut_gamma_veto = "time_gamma_veto >=139 && time_gamma_veto<=145";
    TCut cut_pi_stop = "time_pi_stop_trig>=3 && time_pi_stop_trig <=36";
    TCut cut_time_gamma_sum = "time_gamma_sum_nocalib >=139 && time_gamma_sum_nocalib <=144";
    TCut cut = cut_gamma_veto;
    //reconstruction
    TH2F* h2_reconstruction = new TH2F("h2_reconstruction","energy reconstruction",500,0,220000,500,0,200000);
    t_events->Draw("gamma_sum_nocalib:gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6>>h2_reconstruction",cut,"goff");

    TH1F* h1_reconstruction = new TH1F("h1_reconstruction","reconstructed energy",500,0,220000);
    t_events->Draw("1.135676*gamma_ch1+1.034*gamma_ch2+1.12747*gamma_ch3+1.03648*gamma_ch4+0.632663*gamma_ch5+1.03362*gamma_ch6>>h1_reconstruction",cut,"goff");

    //timing pi-stop
    TH1F* h1_time_pi_stop = new TH1F("h1_time_pi_stop","time pi-stop",512,0,512);
    t_events->Draw("time_pi_stop_trig>>h1_time_pi_stop",cut,"goff");

    //timing electron scintillator 1
    TH1F* h1_time_ele_sci1 = new TH1F("h1_time_ele_sci1","time electron scintillator 1",512,0,512);
    t_events->Draw("time_ele_sci1>>h1_time_ele_sci1",cut,"goff");

    //timing electron scintillator 2
    TH1F* h1_time_ele_sci2 = new TH1F("h1_time_ele_sci2","time electron scintillator 2",512,0,512);
    t_events->Draw("time_ele_sci2>>h1_time_ele_sci2",cut,"goff");

    //coincidence electron scintillators
    TH1F* h1_ele_coincidence = new TH1F("h1_ele_coincidence","time ele_sci2-ele_sci1",512,-256,256);
    t_events->Draw("time_ele_sci2-time_ele_sci1>>h1_ele_coincidence",cut,"goff");

    TH2F* h2_ele_coincidence = new TH2F("h2_ele_coincidence","timing electron scintillators",512,0,512,512,0,512);
    t_events->Draw("time_ele_sci1:time_ele_sci2>>h2_ele_coincidence",cut,"goff");

    //timing gamma colorimeter
    TH1F* h1_time_gamma = new TH1F("h1_time_gamma_sum","time_gamma_sum_nocalib",512,0,512);
    t_events->Draw("time_gamma_sum_nocalib>>h1_time_gamma_sum",cut,"goff");

    TH2F* h2_time_gamma = new TH2F("h2_time_gamma_sum","time and energy gamma sum",512,0,512,500,0,220000);
    t_events->Draw("gamma_sum_nocalib:time_gamma_sum_nocalib>>h2_time_gamma_sum",cut,"goff");
}
