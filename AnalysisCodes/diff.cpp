#include "TFile.h"
#include "TString.h"
#include <iostream>
#include "TTree.h"
#include "TCanvas.h"
#include "TH1.h"
#include <sstream>



void diff()
{
// Variables Declaration

	string file1, file2, leaf1, leaf2;
	double factor;
	int nbins;
	int bins;
	std::ostringstream hfile1stream;
	std::ostringstream hfile2stream;
	std::ostringstream draw1stream;
	std::ostringstream draw2stream;
	std::ostringstream file1pathstream;
	std::ostringstream file2pathstream;

// Input
/*
	std::cout << "First file name (for example: run_1000.root) " << std::endl;
	std::cin >> file1;
	std::cout << "First leaf name (for example: gamma_ch1) "<< std::endl;
	std::cin >> leaf1;
	std::cout << "Second file name"<< std::endl;
	std::cin >> file2;
	std::cout << "Second leaf name"<< std::endl;
	std::cin >> leaf2;
	std::cout << "Add/Subtract Factor (for example: -1.0 means Hist1 - 1.0*Hist2 )"<<std::endl;
	std::cin >> factor;
	std::cout << "Number of Bins (Integer between 1 and inf)"<<std::endl;
	std::cin >> bins;
	
*/
// SAMPLE FOR FAST DEBUGGING (Comment Input part and uncomment this part to set it up.)
	file1 = "run_1246.root";
	file2 = "run_1247.root";
	leaf1 = "gamma_ch1";
	leaf2 = "gamma_ch1";
	factor = -2.04;
	bins = 500;

	
// Setup

	file1pathstream << "./../rootNew/" << file1;			// File Path
	file2pathstream << "./../rootNew/" << file2;
	const char *file1path = file1pathstream.str().c_str();
	const char *file2path = file2pathstream.str().c_str();
	
	
	const char* leaf1name = leaf1.c_str();
	const char* leaf2name = leaf2.c_str();
	const char* file1name = file1.c_str();
	const char* file2name = file2.c_str();  
	
	hfile1stream << "h_" << leaf1;
	hfile2stream << "x_" << leaf2;
	const char* hfile1 = hfile1stream.str().c_str(); // hfile1 = "h_gamma_ch1" for example
	const char* hfile2 = hfile2stream.str().c_str();
    
   	draw1stream << leaf1 << ">>" << hfile1;
	const char* draw1char = draw1stream.str().c_str(); // draw1char = "gamma_ch1>>h_gamma_ch1" for example
	draw2stream << leaf2 << ">>" << hfile2 ;
	const char* draw2char = draw2stream.str().c_str();

// Open/Create files and read tree

	TFile* if1 = new TFile(file1path, file1name);
	TTree* t1 = (TTree*)if1->Get("events");

	TFile* if2 = new TFile(file2path, file2name);
	TTree* t2 = (TTree*)if2->Get("events");
	
	TFile* of = new TFile("diff.root", "RECREATE");

// Find Upper End (maximum) of the x-axis

	Double_t max1 = t1->GetMaximum(leaf1name);
	Double_t max2 = t2->GetMaximum(leaf2name);

	if (max1>=max2){nbins = max1+1;}
	else {nbins = max2+1;}
	
	float nentries1 = t1->GetEntries();
	float nentries2 = t2->GetEntries();
	
	std::cout << "Ratio of Entries (#entries1 / #entries2) = " << nentries1/nentries2 << std::endl;
	
// Histograms
	
	TH1F* h1 = new TH1F(hfile1,"Histogram 1",bins, 0, nbins);
	TH1F* h2 = new TH1F(hfile2,"Histogram 2",bins, 0, nbins);
	

	t1->Draw(draw1char,"","goff");
	t2->Draw(draw2char,"","goff");

// MAIN	

	TH1F* h3 = (TH1F*)h1->Clone("h_diff"); 
	h3->SetTitle("h_diff");
	h3->Add(h2, factor); //  adds factor*h2 to h1.

// Save & Write

	of->WriteTObject(h3);
	of->WriteTObject(h1);
	of->WriteTObject(h2);
	
// Close or Delete

	if1->Delete();
	if2->Delete();
	of->Delete();
	
// Print "Done"

	printf("%s\n","Done. See file diff.root");
}