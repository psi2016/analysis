#include <iostream>
#include <sstream>

#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFractionFitter.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TH1.h"
#include "THStack.h"
#include "TLegend.h"
#include "TVirtualFitter.h"
#include "TCut.h"

void subtract_background()
{
    //define fit range
    Double_t range_low  = 35000;
    Double_t range_high = 80000;

    //define properties of histograms
    Int_t hist_min=0;
    Int_t hist_max=220000;
    Int_t hist_nbins=100;

    //get Files with data
    TFile* f_signal = new TFile("./../rootNew/gamma_pi-stop/sum_gamma_pi-stop.root");
    TFile* f_cosmics = new TFile("./../rootNew/cosmics/cosmics_sum_1494to1498.root");
    TFile* f_michel = new TFile("./../rootNew/michel/michel_sum.root");
    TFile* f_notarget = new TFile("./../rootNew/no_target_gamma_pi-stop/no_target_gamma_pi-stop_sum.root");

    //create output file
    TFile* f_out = new TFile ("./../rootNew/background_subtracted/clean_sum_gamma_pi-stop.root","RECREATE");

    //define cuts and objects to draw
    Double_t w1=1.13576;                      //weights for channels
    Double_t w2=1.034;
    Double_t w3=1.12747;
    Double_t w4=1.03648;
    Double_t w5=0.632663;
    Double_t w6=1.03362;
    Double_t rec_upper = 1.2;           //check if channels were reconstructed correctly
    Double_t rec_lower = 0.8;
    Double_t time_lower = 142;          //time cut
    Double_t time_upper = 146;
    Double_t time_ele_sci1_low=0;     //time cut on electron signal
    Double_t time_ele_sci1_up=1000;
    Double_t time_ele_sci2_low=0;
    Double_t time_ele_sci2_up=1000;

    //define selection what to draw
    std::ostringstream selstream;
    selstream << w1 << "*gamma_ch1+" << w2 << "*gamma_ch2+" << w3 << "*gamma_ch3+" << w4 << "*gamma_ch4+" << w5 << "*gamma_ch5+" << w6 <<"*gamma_ch6";
    const char* sel = selstream.str().c_str();

    std::ostringstream rec_cutstream;
    rec_cutstream << "("<< selstream.str() << ")/gamma_sum_nocalib > " << rec_lower << " && (" << selstream.str() << ")/gamma_sum_nocalib < " << rec_upper;
    TCut rec_cut = rec_cutstream.str().c_str();

    std::ostringstream time_cutstream;
    time_cutstream << "time_gamma_sum_nocalib >=" << time_lower << " && time_gamma_sum_nocalib <=" << time_upper;
    TCut time_cut = time_cutstream.str().c_str();

    std::ostringstream ele_sci1cutstream;
    ele_sci1cutstream << "time_ele_sci1>=" << time_ele_sci1_low << " && time_ele_sci1<=" << time_ele_sci1_up;
    TCut ele_sci1cut = ele_sci1cutstream.str().c_str();

    std::ostringstream ele_sci2cutstream;
    ele_sci2cutstream << "time_ele_sci2>=" << time_ele_sci2_low << " && time_ele_sci2<=" << time_ele_sci2_up;
    TCut ele_sci2cut = ele_sci2cutstream.str().c_str();

    TCut cut = rec_cut && time_cut && ele_sci1cut && ele_sci2cut;


    //Set trees
    TTree* t_signal = (TTree*)f_signal->Get("events");
    t_signal->SetBranchStatus("*",0);
    t_signal->SetBranchStatus("gamma_ch1",1);
    t_signal->SetBranchStatus("gamma_ch2",1);
    t_signal->SetBranchStatus("gamma_ch3",1);
    t_signal->SetBranchStatus("gamma_ch4",1);
    t_signal->SetBranchStatus("gamma_ch5",1);
    t_signal->SetBranchStatus("gamma_ch6",1);
    t_signal->SetBranchStatus("gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_ele_sci1",1);
    t_signal->SetBranchStatus("time_ele_sci2",1);

    TTree* t_cosmics = (TTree*)f_cosmics->Get("events");
    t_cosmics->SetBranchStatus("*",0);
    t_cosmics->SetBranchStatus("gamma_ch1",1);
    t_cosmics->SetBranchStatus("gamma_ch2",1);
    t_cosmics->SetBranchStatus("gamma_ch3",1);
    t_cosmics->SetBranchStatus("gamma_ch4",1);
    t_cosmics->SetBranchStatus("gamma_ch5",1);
    t_cosmics->SetBranchStatus("gamma_ch6",1);
    t_cosmics->SetBranchStatus("gamma_sum_nocalib",1);
    t_cosmics->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_cosmics->SetBranchStatus("time_ele_sci1",1);
    t_cosmics->SetBranchStatus("time_ele_sci2",1);

    TTree* t_michel = (TTree*)f_michel->Get("events");
    t_michel->SetBranchStatus("*",0);
    t_michel->SetBranchStatus("gamma_ch1",1);
    t_michel->SetBranchStatus("gamma_ch2",1);
    t_michel->SetBranchStatus("gamma_ch3",1);
    t_michel->SetBranchStatus("gamma_ch4",1);
    t_michel->SetBranchStatus("gamma_ch5",1);
    t_michel->SetBranchStatus("gamma_ch6",1);
    t_michel->SetBranchStatus("gamma_sum_nocalib",1);
    t_michel->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_michel->SetBranchStatus("time_ele_sci1",1);
    t_michel->SetBranchStatus("time_ele_sci2",1);

    TTree* t_notarget = (TTree*)f_notarget->Get("events");
    t_notarget->SetBranchStatus("*",0);
    t_notarget->SetBranchStatus("gamma_ch1",1);
    t_notarget->SetBranchStatus("gamma_ch2",1);
    t_notarget->SetBranchStatus("gamma_ch3",1);
    t_notarget->SetBranchStatus("gamma_ch4",1);
    t_notarget->SetBranchStatus("gamma_ch5",1);
    t_notarget->SetBranchStatus("gamma_ch6",1);
    t_notarget->SetBranchStatus("gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_ele_sci1",1);
    t_notarget->SetBranchStatus("time_ele_sci2",1);

    //create histograms
    TH1F* h_signal = new TH1F("h_signal","h_signal",hist_nbins,hist_min,hist_max);
    TH1F* h_cosmics = new TH1F("h_cosmics","h_cosmics",hist_nbins,hist_min,hist_max);
    TH1F* h_michel = new TH1F("h_michel","h_michel",hist_nbins,hist_min,hist_max);
    TH1F* h_notarget = new TH1F("h_notarget","h_notarget",hist_nbins,hist_min,hist_max);

    //get histograms
    std::ostringstream get_signal_stream;
    get_signal_stream << sel << ">> h_signal";
    const char* get_signal = get_signal_stream.str().c_str();

    std::ostringstream get_cosmics_stream;
    get_cosmics_stream << sel << ">> h_cosmics";
    const char* get_cosmics = get_cosmics_stream.str().c_str();

    std::ostringstream get_michel_stream;
    get_michel_stream << sel << ">> h_michel";
    const char* get_michel = get_michel_stream.str().c_str();

    std::ostringstream get_notarget_stream;
    get_notarget_stream << sel << ">> h_notarget";
    const char* get_notarget = get_notarget_stream.str().c_str();

    t_signal->Draw(get_signal,rec_cut+ele_sci1cut+ele_sci2cut+time_cut,"goff");
    t_cosmics->Draw(get_cosmics,rec_cut,"goff");
    t_michel->Draw(get_michel,rec_cut+time_cut,"goff");
    t_notarget->Draw(get_notarget,rec_cut+time_cut,"goff");

    //Set TFractionFitter
    TObjArray* templates = new TObjArray(3);
    templates->Add(h_notarget);
    templates->Add(h_cosmics);
    templates->Add(h_michel);

    TFractionFitter* fitter = new TFractionFitter(h_signal,templates);
    fitter->Constrain(0,0.0,1.0);
    fitter->Constrain(1,0.0,1.0);
    fitter->Constrain(2,0.0,1.0);
    fitter->GetFitter()->SetParameter(0,"frac_notarget",0.1,0.05,0.0,1.0);
    fitter->GetFitter()->SetParameter(1,"frac_cosmics",0.00,0.02,0.0,1.0);
    fitter->GetFitter()->SetParameter(2,"frac_michel",0.0,0.02,0.0,1.0);

    Int_t bin_low = h_signal->FindBin(range_low);
    Int_t bin_high = h_signal->FindBin(range_high);
    fitter->SetRangeX(bin_low,bin_high);

    //perform fit
    Int_t status = fitter->Fit();
    std::cout << "fit status: " << status << std::endl;

    //get fractions in fit range
    Double_t frac_notarget;
    Double_t frac_notarget_err;
    Double_t frac_cosmics;
    Double_t frac_cosmics_err;
    Double_t frac_michel;
    Double_t frac_michel_err;
    fitter->GetResult(0,frac_notarget,frac_notarget_err);
    fitter->GetResult(1,frac_cosmics,frac_cosmics_err);
    fitter->GetResult(2,frac_michel,frac_michel_err);

    //get scale factors
    Double_t norm_signal = h_signal->Integral(bin_low,bin_high);
    Double_t norm_notarget = h_notarget->Integral(bin_low,bin_high);
    Double_t norm_cosmics = h_cosmics->Integral(bin_low,bin_high);
    Double_t norm_michel = h_michel->Integral(bin_low,bin_high);

    Double_t scale_notarget = frac_notarget*norm_signal/norm_notarget;
    Double_t scale_cosmics = frac_cosmics*norm_signal/norm_cosmics;
    Double_t scale_michel = frac_michel*norm_signal/norm_michel;

    std::cout << "scale notarget:" << scale_notarget << "   scale cosmics:" << scale_cosmics << "   scale michel" << scale_michel << std::endl;
    std::cout << "norm_signal: " << norm_signal << "    norm_notarget: " << norm_notarget << "  norm_cosmics: " << norm_cosmics << "    norm_michel: " << norm_michel << std::endl;

    //subtract background histograms
    TH1F* h_clean_signal = (TH1F*)h_signal->Clone("h_clean_signal");
    h_clean_signal->Add(h_notarget,-scale_notarget);
    h_clean_signal->Add(h_cosmics,-scale_cosmics);
    h_clean_signal->Add(h_michel,-scale_michel);



    //draw output
    f_out->cd();
    h_notarget->Scale(scale_notarget);
    h_cosmics->Scale(scale_cosmics);
    h_michel->Scale(scale_michel);
    TCanvas* c_out = new TCanvas("results","results");
    c_out->Divide(2,1);
    c_out->cd(1);
    gPad->SetLogy();

    THStack* h_stack = new THStack("backgrounds","backgrounds");
    h_stack->Add(h_michel);
    h_stack->Add(h_cosmics);
    h_stack->Add(h_notarget);

    h_notarget->SetFillColor(kBlue);
    h_cosmics->SetFillColor(kRed);
    h_michel->SetFillColor(kGreen);

    TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
    leg->AddEntry(h_signal,"taken data","l,e");
    leg->AddEntry(h_notarget,"background without target","f");
    leg->AddEntry(h_cosmics,"cosmics","f");
    leg->AddEntry(h_michel,"michel","f");
    h_stack->Draw();
    h_signal->DrawClone("ep,SAME");
    leg->Draw("same");

    c_out->cd(2);
    h_clean_signal->Draw();

    //save output
    f_out->WriteTObject(c_out);
    f_out->WriteTObject(h_clean_signal);
    f_out->WriteTObject(h_stack);
    f_out->WriteTObject(h_signal);
    f_out->WriteTObject(h_notarget);
    f_out->WriteTObject(h_cosmics);
    f_out->WriteTObject(h_michel);

    //close files and canvas
    c_out->Close();
    f_signal->Close();
    f_notarget->Close();
    f_cosmics->Close();
    f_michel->Close();
    f_out->Close();
}
