#include <iostream>
#include <sstream>

#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFractionFitter.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TH1.h"
#include "THStack.h"
#include "TLegend.h"
#include "TVirtualFitter.h"
#include "TCut.h"

void subtract_notarget_michel()
{
    //define fit range
    Double_t range_low  = 20000;
    Double_t range_high = 40000;

    //define properties of histograms
    Int_t hist_min=0;
    Int_t hist_max=220000;
    Int_t hist_nbins=100;

    //get Files with data
    TFile* f_signal = new TFile("./../rootNew/michel/michel_1245to1247.root");

    TFile* f_notarget = new TFile("./../rootNew/no_target_gamma/no_target_gamma_sum.root");

    //create output file
    TFile* f_out = new TFile ("./../rootNew/background_subtracted/michel_1245to1247_minus_notarget.root","RECREATE");

    //define cuts and objects to draw
    Double_t w1=1.13576;                      //weights for channels
    Double_t w2=1.034;
    Double_t w3=1.12747;
    Double_t w4=1.03648;
    Double_t w5=0.632663;
    Double_t w6=1.03362;
    Double_t rec_upper = 1.2;           //check if channels were reconstructed correctly
    Double_t rec_lower = 0.8;
    //signal cuts
    Double_t gamma_time_lower = 140;          //time cut
    Double_t gamma_time_upper = 148;
    Double_t time_pi_stop_low = 142;
    Double_t time_pi_stop_up = 152;
    Double_t time_gamma_veto_low = 160;
    Double_t time_gamma_veto_up = 168;
    //background cuts
    Double_t bck_time_gamma_veto_low=139;
    Double_t bck_time_gamma_veto_up=145;
    //define selection what to draw
    std::ostringstream selstream;
    selstream << w1 << "*gamma_ch1+" << w2 << "*gamma_ch2+" << w3 << "*gamma_ch3+" << w4 << "*gamma_ch4+" << w5 << "*gamma_ch5+" << w6 <<"*gamma_ch6";
    const char* sel = selstream.str().c_str();

    //define reconstruction cut
    std::ostringstream rec_cutstream;
    rec_cutstream << "("<< selstream.str() << ")/gamma_sum_nocalib > " << rec_lower << " && (" << selstream.str() << ")/gamma_sum_nocalib < " << rec_upper;
    TCut rec_cut = rec_cutstream.str().c_str();

    //define cuts for signal
    std::ostringstream gamma_time_cutstream;
    gamma_time_cutstream << "time_gamma_sum_nocalib >=" << gamma_time_lower << " && time_gamma_sum_nocalib <=" << gamma_time_upper;
    TCut gamma_time_cut = gamma_time_cutstream.str().c_str();

    std::ostringstream pi_stopcutstream;
    pi_stopcutstream << "time_pi_stop_trig>=" << time_pi_stop_low << " && time_pi_stop_trig<=" << time_pi_stop_up;
    TCut pi_stopcut = pi_stopcutstream.str().c_str();

    std::ostringstream gamma_vetocutstream;
    gamma_vetocutstream << "time_gamma_veto>=" << time_gamma_veto_low << " && time_gamma_veto<=" << time_gamma_veto_up;
    TCut gamma_vetocut = gamma_vetocutstream.str().c_str();

    TCut signal_cut = rec_cut && gamma_time_cut && pi_stopcut && gamma_vetocut;

    //define cuts for background
    std::ostringstream bck_gamma_vetocutstream;
    bck_gamma_vetocutstream << "time_gamma_veto>=" << bck_time_gamma_veto_low << " && time_gamma_veto<=" << bck_time_gamma_veto_up;
    TCut bck_gamma_vetocut = bck_gamma_vetocutstream.str().c_str();

    TCut bck_cut = rec_cut && bck_gamma_vetocut && gamma_time_cut && pi_stopcut;


    //Set trees
    TTree* t_signal = (TTree*)f_signal->Get("events");
    t_signal->SetBranchStatus("*",0);
    t_signal->SetBranchStatus("gamma_ch1",1);
    t_signal->SetBranchStatus("gamma_ch2",1);
    t_signal->SetBranchStatus("gamma_ch3",1);
    t_signal->SetBranchStatus("gamma_ch4",1);
    t_signal->SetBranchStatus("gamma_ch5",1);
    t_signal->SetBranchStatus("gamma_ch6",1);
    t_signal->SetBranchStatus("gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_ele_sci1",1);
    t_signal->SetBranchStatus("time_ele_sci2",1);
    t_signal->SetBranchStatus("time_gamma_veto",1);
    t_signal->SetBranchStatus("time_pi_stop_trig",1);

    TTree* t_notarget = (TTree*)f_notarget->Get("events");
    t_notarget->SetBranchStatus("*",0);
    t_notarget->SetBranchStatus("gamma_ch1",1);
    t_notarget->SetBranchStatus("gamma_ch2",1);
    t_notarget->SetBranchStatus("gamma_ch3",1);
    t_notarget->SetBranchStatus("gamma_ch4",1);
    t_notarget->SetBranchStatus("gamma_ch5",1);
    t_notarget->SetBranchStatus("gamma_ch6",1);
    t_notarget->SetBranchStatus("gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_ele_sci1",1);
    t_notarget->SetBranchStatus("time_ele_sci2",1);
    t_notarget->SetBranchStatus("time_gamma_veto",1);
    t_notarget->SetBranchStatus("time_pi_stop_trig",1);

    //create histograms without cuts
    TH1F* h_signal = new TH1F("h_signal","h_signal",hist_nbins,hist_min,hist_max);
    TH1F* h_notarget = new TH1F("h_notarget","h_notarget",hist_nbins,hist_min,hist_max);

//    //create histograms with cuts
//    TH1F* h_cut_signal = new TH1F("h_cut_signal","h_signal with cuts",hist_nbins,hist_min,hist_max);
//    TH1F* h_cut_notarget = new TH1F("h_cut_notarget","h_notarget with cuts",hist_nbins,hist_min,hist_max);

    //get histograms
    std::ostringstream get_signal_stream;
    get_signal_stream << sel << ">> h_signal";
    const char* get_signal = get_signal_stream.str().c_str();

    std::ostringstream get_notarget_stream;
    get_notarget_stream << sel << ">> h_notarget";
    const char* get_notarget = get_notarget_stream.str().c_str();

//    std::ostringstream get_cut_signal_stream;
//    get_cut_signal_stream << sel << ">> h_cut_signal";
//    const char* get_cut_signal = get_cut_signal_stream.str().c_str();
//
//    std::ostringstream get_cut_notarget_stream;
//    get_cut_notarget_stream << sel << ">> h_cut_notarget";
//    const char* get_cut_notarget = get_cut_notarget_stream.str().c_str();

    t_signal->Draw(get_signal,signal_cut,"goff");
    t_notarget->Draw(get_notarget,bck_cut,"goff");
//    t_signal->Draw(get_cut_signal,cut,"goff");
//    t_notarget->Draw(get_cut_notarget,cut,"goff");


    Int_t bin_low = h_signal->FindBin(range_low);
    Int_t bin_high = h_signal->FindBin(range_high);




    //get scale factors
    Double_t norm_signal = h_signal->Integral(bin_low,bin_high);
    Double_t norm_notarget = h_notarget->Integral(bin_low,bin_high);


    Double_t scale_notarget = norm_signal/norm_notarget;

    std::cout << "scale notarget:" << scale_notarget << std::endl;
    std::cout << "norm_signal: " << norm_signal << "    norm_notarget: " << norm_notarget<< std::endl;

    //subtract background histograms
    TH1F* h_clean_signal = (TH1F*)h_signal->Clone("h_clean_signal");
    h_clean_signal->Add(h_notarget,-scale_notarget);

    //draw output
    f_out->cd();
    h_notarget->Scale(scale_notarget);
    TCanvas* c_out = new TCanvas("results","results");
    c_out->Divide(2,1);
    c_out->cd(1);
    gPad->SetLogy();

	h_notarget->SetTitle("h_notarget without cuts");
    h_notarget->SetFillColor(kRed);

    TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
    leg->AddEntry(h_signal,"taken data","l,e");
    leg->AddEntry(h_notarget,"background without target","f");
    h_notarget->Draw();
    h_signal->DrawClone("ep,SAME");
    leg->Draw("same");

//    c_out->cd(2);
//    h_cut_notarget->SetFillColor(kRed);
//    h_cut_notarget->Scale(scale_notarget);
//    h_cut_notarget->Draw();
//    h_cut_signal->Draw("same,ep");
//    c_out->Update();
    c_out->cd(2);
    h_clean_signal->Draw();

    //save output
    f_out->WriteTObject(c_out);
    f_out->WriteTObject(h_clean_signal);
    f_out->WriteTObject(h_signal);
    f_out->WriteTObject(h_notarget);
//    f_out->WriteTObject(h_cut_signal);
//    f_out->WriteTObject(h_cut_notarget);


    //close files and canvas
    c_out->Close();
    f_signal->Close();
    f_notarget->Close();
    f_out->Close();
}
