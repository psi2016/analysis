#include <iostream>
#include <sstream>

#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFractionFitter.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TH1.h"
#include "THStack.h"
#include "TLegend.h"
#include "TVirtualFitter.h"
#include "TCut.h"

void data_bkg_comparison()
{

    //define fit range
    Double_t range_low  = 150000;
    Double_t range_high = 230000;
    Double_t range_low_michel = 62000;
    Double_t range_high_michel = 78000;

    //define properties of histograms
    Int_t hist_min=0;
    Int_t hist_max=230000;
    Int_t hist_nbins=200;


    // ---------------------//
    // read files with data //
    // ---------------------//

    TFile* f_signal = new TFile("./../rootNew/gamma_pi-stop/sum_gamma_pi-stop.root");

    TFile* f_notarget = new TFile("./../rootNew/no_target_gamma_pi-stop/no_target_gamma_pi-stop_sum.root");

    TFile* f_michel = new TFile("./../rootNew/michel/michels_shifted_1245to1247.root");

    //create output file
    TFile* f_out = new TFile ("./../rootNew/background_subtracted/data_bkg_comparison.root","RECREATE");

    //define cuts and objects to draw <- (probably outdated...)
    Double_t w1=1.13576;                //weights for channels
    Double_t w2=1.034;
    Double_t w3=1.12747;
    Double_t w4=1.03648;
    Double_t w5=0.632663;
    Double_t w6=1.03362;
    Double_t rec_upper = 1.2;           //check if channels were reconstructed correctly
    Double_t rec_lower = 0.8;
    Double_t time_lower = 142;          //time cut
    Double_t time_upper = 146;
    Double_t time_ele_sci1_low=136;     //time cut on electron signal
    Double_t time_ele_sci1_up=150;
    Double_t time_ele_sci2_low=136;
    Double_t time_ele_sci2_up=150;
    Double_t coincidencewindow = 5;

    //define selection what to draw     [Jens:] I think this part is from Timo, can I get an explanation how it works/what the idea is?
    std::ostringstream selstream;
    selstream << w1 << "*gamma_ch1+" << w2 << "*gamma_ch2+" << w3 << "*gamma_ch3+" << w4 << "*gamma_ch4+" << w5 << "*gamma_ch5+" << w6 <<"*gamma_ch6";
    const char* sel = selstream.str().c_str();

    std::ostringstream rec_cutstream;
    rec_cutstream << "("<< selstream.str() << ")/gamma_sum_nocalib > " << rec_lower << " && (" << selstream.str() << ")/gamma_sum_nocalib < " << rec_upper;
    TCut rec_cut = rec_cutstream.str().c_str();

    std::ostringstream time_cutstream;
    time_cutstream << "time_gamma_sum_nocalib >=" << time_lower << " && time_gamma_sum_nocalib <=" << time_upper;
    TCut time_cut = time_cutstream.str().c_str();

    std::ostringstream ele_sci1cutstream;
    ele_sci1cutstream << "time_ele_sci1>=" << time_ele_sci1_low << " && time_ele_sci1<=" << time_ele_sci1_up;
    TCut ele_sci1cut = ele_sci1cutstream.str().c_str();

    std::ostringstream ele_sci2cutstream;
    ele_sci2cutstream << "time_ele_sci2>=" << time_ele_sci2_low << " && time_ele_sci2<=" << time_ele_sci2_up;
    TCut ele_sci2cut = ele_sci2cutstream.str().c_str();

    std::ostringstream ele_coincidence_cutstream;
    ele_coincidence_cutstream << "TMath::Abs(time_ele_sci2-time_ele_sci1)<=" << coincidencewindow;
    TCut coincidence_cut = ele_coincidence_cutstream.str().c_str();

    TCut cut = rec_cut && time_cut && ele_sci1cut && ele_sci2cut && coincidence_cut;

    //Set trees
    TTree* t_signal = (TTree*)f_signal->Get("events");
    t_signal->SetBranchStatus("*",0);
    t_signal->SetBranchStatus("gamma_ch1",1);
    t_signal->SetBranchStatus("gamma_ch2",1);
    t_signal->SetBranchStatus("gamma_ch3",1);
    t_signal->SetBranchStatus("gamma_ch4",1);
    t_signal->SetBranchStatus("gamma_ch5",1);
    t_signal->SetBranchStatus("gamma_ch6",1);
    t_signal->SetBranchStatus("gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_signal->SetBranchStatus("time_ele_sci1",1);
    t_signal->SetBranchStatus("time_ele_sci2",1);

    TTree* t_notarget = (TTree*)f_notarget->Get("events");
    t_notarget->SetBranchStatus("*",0);
    t_notarget->SetBranchStatus("gamma_ch1",1);
    t_notarget->SetBranchStatus("gamma_ch2",1);
    t_notarget->SetBranchStatus("gamma_ch3",1);
    t_notarget->SetBranchStatus("gamma_ch4",1);
    t_notarget->SetBranchStatus("gamma_ch5",1);
    t_notarget->SetBranchStatus("gamma_ch6",1);
    t_notarget->SetBranchStatus("gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_gamma_sum_nocalib",1);
    t_notarget->SetBranchStatus("time_ele_sci1",1);
    t_notarget->SetBranchStatus("time_ele_sci2",1);
    
    // Michel Trees
    TTree* t_michel = (TTree*)f_michel->Get("t_gamma_sum_manual_r_nocut"); 
    t_michel->SetBranchStatus("t_gamma_sum_manual_r_nocut",1); 				

    TTree* t_michel_cuts = (TTree*)f_michel->Get("t_gamma_sum_manual_r"); 
    t_michel_cuts->SetBranchStatus("t_gamma_sum_manual_r",1); 				
    
    //create histograms without cuts
    TH1F* h_signal = new TH1F("h_signal","h_signal",hist_nbins,hist_min,hist_max);
    TH1F* h_notarget = new TH1F("h_notarget","h_notarget",hist_nbins,hist_min,hist_max);
    TH1F* h_michel_nocut = new TH1F("h_michel_nocut","h_michel_nocut",hist_nbins,hist_min,hist_max);

    //create histograms with cuts
    TH1F* h_cut_signal = new TH1F("h_cut_signal","h_signal with cuts",hist_nbins,hist_min,hist_max);
    TH1F* h_cut_notarget = new TH1F("h_cut_notarget","h_notarget with cuts",hist_nbins,hist_min,hist_max);
    TH1F* h_michel_cuts = new TH1F("h_michel_cuts","h_michel_cuts",hist_nbins,hist_min,hist_max);


    //get histograms
    std::ostringstream get_signal_stream;
    get_signal_stream << sel << ">> h_signal";									// Signal without cuts
    const char* get_signal = get_signal_stream.str().c_str();
    
    std::ostringstream get_cut_signal_stream;
    get_cut_signal_stream << sel << ">> h_cut_signal";							// Signal with cuts
    const char* get_cut_signal = get_cut_signal_stream.str().c_str();

    std::ostringstream get_notarget_stream;
    get_notarget_stream << sel << ">> h_notarget";								// Notarget without cuts
    const char* get_notarget = get_notarget_stream.str().c_str();

    std::ostringstream get_cut_notarget_stream;		
    get_cut_notarget_stream << sel << ">> h_cut_notarget";						// Notarget with cuts
    const char* get_cut_notarget = get_cut_notarget_stream.str().c_str();
    
    std::ostringstream get_michel_stream;
    get_michel_stream << "t_gamma_sum_manual_r_nocut" << ">> h_michel_nocut"; 	// Michels without cuts
    const char* get_michel = get_michel_stream.str().c_str();
    
    std::ostringstream get_michel_cut_stream;
    get_michel_cut_stream << "t_gamma_sum_manual_r" << ">> h_michel_cuts"; 		// Michels with cuts
    const char* get_michel_cut = get_michel_cut_stream.str().c_str();


	// Draw trees
	t_signal->Draw(get_signal,"","goff");		// Added no cut histograms
    t_notarget->Draw(get_notarget,"","goff");	
    
    t_signal->Draw(get_signal,rec_cut,"goff");
    t_notarget->Draw(get_notarget,rec_cut,"goff");

    t_signal->Draw(get_cut_signal,cut,"goff");
    t_notarget->Draw(get_cut_notarget,cut,"goff");

	t_michel->Draw(get_michel,"","goff");
	t_michel_cuts->Draw(get_michel_cut,"","goff");

   
   
   
    // ----------------------//
    // get the scale factors //
    // ----------------------//

	std::cout <<  "---------------- " << std::endl << "No Target Normalization: " <<  std::endl << "---------------- " << std::endl;
	
    Int_t bin_low = h_signal->FindBin(range_low);
    Int_t bin_high = h_signal->FindBin(range_high);

    std::cout << "Range_low = " << range_low << "   ==>  " << "bin_low = " << bin_low << std::endl;
    std::cout << "Range_high = " << range_high << "  ==>  " << "bin_high = " << bin_high << std::endl;

    //calculate scale factors
    Double_t norm_signal = h_signal->Integral(bin_low,bin_high);
    Double_t norm_notarget = h_notarget->Integral(bin_low,bin_high);
    Double_t scale_notarget = norm_signal/norm_notarget;

    std::cout << std::endl << "scale notarget: " << scale_notarget << std::endl;
    std::cout << "norm_signal: " << norm_signal << std::endl;
    std::cout << "norm_notarget: " << norm_notarget<< std::endl;

	// Michels scale factor
	Int_t bin_low_michel = h_signal->FindBin(range_low_michel);
    Int_t bin_high_michel = h_signal->FindBin(range_high_michel);
    
    std::cout << "---------------- " << std::endl << "Michel Normalization: " <<  std::endl << "---------------- " << std::endl;
    std::cout << "Range_low_michel = " << range_low_michel << "   ==>  " << "bin_low Michels = " << bin_low_michel << std::endl;
    std::cout << "Range_high_michel = " << range_high_michel << "  ==>  " << "bin_high Michels = " << bin_high_michel << std::endl;

	//calculate Michel scale factors
    Double_t norm_signal = h_signal->Integral(bin_low_michel,bin_high_michel);
    Double_t norm_michel = h_michel_nocut->Integral(bin_low_michel,bin_high_michel);
    Double_t scale_michel = norm_signal/norm_michel;

    std::cout << std::endl << "scale michel: " << scale_michel << std::endl;
    std::cout << "norm_signal: " << norm_signal << std::endl;
    std::cout << "norm_michel: " << norm_michel << std::endl;

    
    
    
    // -------------------------------//
    // subtract background histograms //
    // -------------------------------//
    TH1F* h_clean_signal = (TH1F*)h_cut_signal->Clone("h_clean_signal");
    h_clean_signal->Add(h_cut_notarget,-scale_notarget);


    // ------------//
    // draw output //
    // ------------//

    f_out->cd();
    h_notarget->Scale(scale_notarget);
    h_michel_nocut->Scale(scale_michel);
    //h_michel_cuts->Scale(scale_michel);


    TCanvas* c_out = new TCanvas("results","results");
    //c_out->Divide(3,2);
    c_out->Divide(2,2);

    //raw signal compared to notarget background + michel (w/o cuts)
    c_out->cd(1);
    gPad->SetLogy();
	
    h_notarget->SetTitle("h_notarget without cuts");
    h_notarget->SetFillColor(kRed);

    h_michel_nocut->SetTitle("h_michel without cuts");
    h_michel_nocut->SetFillColor(kBlue);

    TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
    leg->AddEntry(h_signal,"taken data","l,e");
    leg->AddEntry(h_notarget,"background without target","f");
    leg->AddEntry(h_michel_nocut,"Michel spectrum (no cuts, runs 1245to1247)","f");

    // Use the concept of "THStack" here. For a simple example refer to
    // https://root.cern.ch/doc/master/classTHStack.html
    THStack *hs_raw = new THStack("hs_raw","");
    hs_raw->Add(h_notarget);
    hs_raw->Add(h_michel_nocut);

    h_signal->Draw();
    hs_raw->Draw("same");
    leg->Draw("same");
    h_signal->Draw("same");
    
     c_out->cd(2);
	
    h_notarget->SetTitle("h_notarget without cuts");
    h_notarget->SetFillColor(kRed);

    h_michel_nocut->SetTitle("h_michel without cuts");
    h_michel_nocut->SetFillColor(kBlue);

    TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
    leg->AddEntry(h_signal,"taken data","l,e");
    leg->AddEntry(h_notarget,"background without target","f");
    leg->AddEntry(h_michel_nocut,"Michel spectrum (no cuts, runs 1245to1247)","f");

    // Use the concept of "THStack" here. For a simple example refer to
    // https://root.cern.ch/doc/master/classTHStack.html
    THStack *hs_raw = new THStack("hs_raw","");
    hs_raw->Add(h_notarget);
    hs_raw->Add(h_michel_nocut);

    h_signal->Draw();
    hs_raw->Draw("same");
    leg->Draw("same");
    h_signal->Draw("same");

    c_out->cd(3);
    gPad->SetLogy();
    h_michel_nocut->Draw();

    c_out->cd(4);
    gPad->SetLogy();
    h_notarget->Draw();


    //save output
    f_out->WriteTObject(c_out);
    f_out->WriteTObject(h_clean_signal);
    f_out->WriteTObject(h_signal);
    f_out->WriteTObject(h_notarget);
    f_out->WriteTObject(h_cut_signal);
    f_out->WriteTObject(h_cut_notarget);
    f_out->WriteTObject(h_michel_nocut);
  	f_out->WriteTObject(h_michel_cuts);


   	//Print "Done"
    std::cout << "----------------" << std::endl;
    std::cout << "Done. See file ./../rootNew/background_subtracted/data_bkg_comparison.root" << std::endl;


	return; // REMOVE THIS IF NOT DEBUGGING! IF ACTIVE, PROGRAM STOPS HERE AND FILES WILL NOT CLOSE.
	
    //close files and canvas
    c_out->Close();
    f_signal->Close();
    f_notarget->Close();
    f_michel->Close();
    f_out->Close();
   

    
}
