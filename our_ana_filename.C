void our_ana_filename() {
	string filena;
	std::ostringstream filepathstream;
	
	std::cout <<" Filename = ";
	std::cin  >> filena;

	
	filepathstream << filena;	
	const char *filepath = filepathstream.str().c_str();
	const char* filename = filena.c_str();
	
	
    TFile* file = new TFile(filepath, filename);
    TTree* tree = (TTree*)file->Get("events");

    gStyle->SetOptStat(0);

    TCanvas* c1= new TCanvas("c1","c1",1920,1080);
    c1->Divide(2,2);
    TCanvas* c2= new TCanvas("c2","c2",1920,1080);
    c2->Divide(2,2);
    TCanvas* c3= new TCanvas("c3","c3",1920,1080);

    //no cuts
    c1->cd(1);
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_nocalib(1000,0,130e3)");
    h_gamma_sum_nocalib->GetXaxis()->SetTitle("gamma energy [A.E.]");

    c1->cd(2);
    tree->Draw("gamma_sum_nocalib:time_pi_stop_trig>>h_gamma_sum_pi_stop(512,0,512,1000,0,130e3)","","colz");
    h_gamma_sum_pi_stop->GetYaxis()->SetTitle("gamma energy [A.E.]");
    h_gamma_sum_pi_stop->GetXaxis()->SetTitle("time [10ns]");

    // (1) cut on time of pi_stop trigger
    c1->cd(1);
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_time_cut(1000,0,130e3)","time_pi_stop_trig>135&&time_pi_stop_trig<148","same");
    h_gamma_sum_time_cut->SetLineColor(kRed);

    c1->cd(3);
    tree->Draw("gamma_sum_nocalib:time_gamma_sum_nocalib>>h_gamma_sum_gamma_time(512,0,512,1000,0,130e3)","","colz");

    c1->cd(4);
    tree->Draw("time_gamma_sum_nocalib>>h_gamma_time(512,0,512)");

    // (2) cut on time of gamma calo
    c1->cd(1);
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_time_cut_gamma_cut(1000,0,130e3)","time_gamma_sum_nocalib>134&&time_gamma_sum_nocalib<145","same");
    h_gamma_sum_time_cut_gamma_cut->SetLineColor(kGreen+2);

    /// /// /// /// /// ///

    c2->cd(1);
    tree->Draw("peak_ele_sci1>>h_peak_ele_sci1(1000,0,100)");
    c2->cd(2);
    tree->Draw("peak_ele_sci2>>h_peak_ele_sci2(1000,0,1000)");

    // (3) cut on el1 and el2 (coincidence)
    c1->cd(1);
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_time_cut_gamma_cut_el1el2(1000,0,130e3)","peak_ele_sci1>40&&peak_ele_sci2>140","same");
    h_gamma_sum_time_cut_gamma_cut_el1el2->SetLineColor(kOrange+2);
    gPad->SetLogy();

    // cut (1) && cut (2)
    c1->cd(1);
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_pi_stop_time_cut_gamma_cut(1000,0,130e3)","time_pi_stop_trig>135&&time_pi_stop_trig<148&&time_gamma_sum_nocalib>134&&time_gamma_sum_nocalib<145","same");
    h_gamma_sum_pi_stop_time_cut_gamma_cut->SetLineColor(kYellow+2);

    // ALL cuts
    tree->Draw("gamma_sum_nocalib>>h_gamma_sum_all_cuts(1000,0,130e3)","time_pi_stop_trig>135&&time_pi_stop_trig<148&&time_gamma_sum_nocalib>134&&time_gamma_sum_nocalib<145&&peak_ele_sci1>40&&peak_ele_sci2>145&&time_ele_sci1>134&&time_ele_sci1<145&&time_ele_sci2>133&&time_ele_sci2<145","same");
    h_gamma_sum_all_cuts->SetLineColor(kBlue-2);

    //draw again to save as pdf
    c3->cd();

    //no cuts
    h_gamma_sum_nocalib->Draw();

    // cut (1) && cut (2)
    h_gamma_sum_pi_stop_time_cut_gamma_cut->SetLineColor(kRed);
    h_gamma_sum_pi_stop_time_cut_gamma_cut->Draw("same");

    // ALL cuts
    h_gamma_sum_all_cuts->SetLineColor(kGreen+2);
    h_gamma_sum_all_cuts->Draw("same");


    gPad->SetLogy();

    // estimate number of gammas
    TF1* bkg = new TF1("bkg","expo",30e3,110e3);
    bkg->SetLineColor(kBlue);
    h_gamma_sum_pi_stop_time_cut_gamma_cut->Fit(bkg,"","",30e3,65e3);
    //h_gamma_sum_pi_stop_time_cut_gamma_cut->Fit("expo");

    int bin = h_gamma_sum_pi_stop_time_cut_gamma_cut->FindBin(85e3);
    int binStart = bin;
    cout << "bkg->Eval(85e3) = " << bkg->Eval(85e3) << endl;
    while(h_gamma_sum_pi_stop_time_cut_gamma_cut->GetBinContent(bin) > bkg->Eval(h_gamma_sum_pi_stop_time_cut_gamma_cut->GetBinCenter(bin))) {
        bin++;
    }

    double bkg_gammas = bkg->Integral(85e3,h_gamma_sum_pi_stop_time_cut_gamma_cut->GetBinCenter(bin))/h_gamma_sum_pi_stop_time_cut_gamma_cut->GetBinWidth(1);
    double sig_bkg_gammas = h_gamma_sum_pi_stop_time_cut_gamma_cut->Integral(binStart,bin);
    double sig_gammas = sig_bkg_gammas - bkg_gammas;

    double n_ele = h_gamma_sum_all_cuts->Integral(binStart,bin);

    cout << "intersection at bin = " << bin << endl;
    cout << "intersection at E = " << h_gamma_sum_pi_stop_time_cut_gamma_cut->GetBinCenter(bin) << endl;

    cout << "sig+bkg gammas = " << sig_bkg_gammas << endl;
    cout << "bkg gammas = " << bkg_gammas << endl;
    cout << "sig gammas = " << sig_gammas << endl;
    cout << "n_ele = " << n_ele << endl;
    cout << "n_ele/sig gammas = " << n_ele/sig_gammas << endl;

    TLegend* leg = new TLegend(0.7, 0.6, 0.9, 0.9);
    leg->AddEntry(h_gamma_sum_nocalib,"no cuts", "l");
    leg->AddEntry(h_gamma_sum_pi_stop_time_cut_gamma_cut,"time cuts on gamma calo & pi-stop", "l");
    leg->AddEntry(h_gamma_sum_all_cuts,"time cuts & e coincidence", "l");
    leg->AddEntry((TObject*)0,Form("sig+bkg gammas = %f", sig_bkg_gammas), "");
    leg->AddEntry((TObject*)0,Form("bkg gammas = %f", bkg_gammas), "");
    leg->AddEntry((TObject*)0,Form("sig gammas= %f", sig_gammas), "");
    leg->AddEntry((TObject*)0,Form("n_ele = %f", n_ele), "");
    leg->AddEntry((TObject*)0,Form("n_ele/sig gammas = %f", n_ele/sig_gammas), "");
    leg->Draw();

    c3->SaveAs("gamma_energy.pdf");
    c1->Close();
    c2->Close();

    return;
}
