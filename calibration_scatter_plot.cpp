

//code gets the weights of the channels from a linear regression of the scatter plot gamma_chi:gamma_chj

#include "TFile.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TH2.h"
#include "TGraph.h"
#include "TCut.h"
#include "TStyle.h"
#include <sstream>
#include <iostream>
#include "TF1.h"
using namespace std;

void calibration_scatter_plot(const char* infilename)
{
    //create output file
    std::ostringstream outfilestream;
    outfilestream << "./../AnalysisRoot/calibration_scatter_" << infilename ;
    const char* outfilechar = outfilestream.str().c_str();
    TFile* outfile = new TFile(outfilechar,"RECREATE");

    //get input file
    std::ostringstream infilestream;
    infilestream << "./../rootNew/gamma_pi-stop/" << infilename ;
    const char* infilechar = infilestream.str().c_str();
    TFile* infile = new TFile(infilechar,"READ");

    //get TTree
    TTree* events = (TTree*)infile->Get("events");

    //create canvases
    TCanvas* c1 = new TCanvas("c_channel1","channel_1");
    TCanvas* c2 = new TCanvas("c_channel2","channel_2");
    TCanvas* c3 = new TCanvas("c_channel3","channel_3");
    TCanvas* c4 = new TCanvas("c_channel4","channel_4");
    TCanvas* c5 = new TCanvas("c_channel5","channel_5");
    TCanvas* c6 = new TCanvas("c_channel6","channel_6");
    TCanvas* c_temp;                                        //pointer to set on current canvas in loop

    //divde canvases. One pad per channel. ci(j) for scatter plot ch_i:ch_j
    c1->Divide(3,2);
    c2->Divide(3,2);
    c3->Divide(3,2);
    c4->Divide(3,2);
    c5->Divide(3,2);
    c6->Divide(3,2);

    gStyle->SetOptFit(1);       //Draw fit result into the canvas

    //create linear function
    TF1* lin = new TF1("lin","[0]*x",0,500000);
    lin->SetParameter(0,1.);

    //array for weights from fits
    double weights[6][6];

    //more arrays for calculation
    double weightsrel[6][6][6];     //[i][k][j] weight of channel [k] relative to channel[i] obtained by channel[j]
    double meanweights[6][6];       //[i][k] mean weight of channel [k] relative to channel [i]
    double normweights[6];          //sum of the weights of all channel [k] relative to channel [i]
    double finalweights[6];         //final weight of channel k

    //define cuts
    double rec_max=1.1;
    double rec_min=0.9;
    TCut rec_max_cut = "(gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib<1.1";
    TCut rec_min_cut = "(gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib>0.9";
    TCut rec_cut = rec_max_cut&&rec_min_cut;

    //fit all the scatter plots to obtain the ration weight_chi/weight_chj
    for(int i = 1; i<=6;i++)            //loop over channel i
    {
        //go to current canvas
        if(i==1){c_temp=c1;}
        if(i==2){c_temp=c2;}
        if(i==3){c_temp=c3;}
        if(i==4){c_temp=c4;}
        if(i==5){c_temp=c5;}
        if(i==6){c_temp=c6;}

         //get name of first branch
        std::ostringstream xstream;
        xstream << "gamma_ch"<< i;
        const char* xname = xstream.str().c_str();

        //get maximum x for drawing
        double xmax = events->GetMaximum(xname);

        for(int j = 1; j<=6;j++)         //loop over channel j
        {
            //go into right pad
            c_temp->cd(j);

            //get name of second branch
            std::ostringstream ystream;
            ystream << "gamma_ch"<< j;
            const char* yname = ystream.str().c_str();

            //get maximum y for drawing
            double ymax = events->GetMaximum(yname);

            //create histogram for scatter plot
            std::ostringstream histstream;
            histstream << "gamma_ch"<< i<<"_vs_gamma_ch" <<j ;
            const char* histname = histstream.str().c_str();
            TH2D* scatterplot = new TH2D(histname,histname,1000,0,xmax,1000,0,ymax);

            //draw scatter plot
            std::ostringstream scatterstream;
            scatterstream << "gamma_ch"<< i<<":gamma_ch" <<j << ">>" << histstream.str();
            const char* scatterchar = scatterstream.str().c_str();
            events->Draw(scatterchar,rec_cut);

            //fit with linear function
            scatterplot->Fit("lin");

            //save weight
            weights[i-1][j-1]=lin->GetParameter(0);
        }
    }

    //calculate the weights on all possible ways and take the average
    //calculate weightsrel[i][k][j] weight of channel [k] relative to channel[i] obtained by channel[j]
    for (int i=0;i<6;i++)
    {
        for (int k=0;k<6;k++)
        {
            for (int j=0; j<6;j++)
            {
                weightsrel[i][k][j]=weights[i][j]*weights[j][k];
            }
        }
    }

    //average over intermediate weight j
    for (int i=0; i<6;i++)
    {
        normweights[i]=0;         //for normalization(see line 152ff)
        for(int k=0;k<6;k++)
        {
            meanweights[i][k]=0;
            for(int j=0; j<6;j++)
            {
                meanweights[i][k]+=weightsrel[i][k][j]/6.;
            }
            normweights[i]+=meanweights[i][k];
        }
    }
    //normalize so that the sum is equal to 6 and print result on screen
    for(int i=0; i<6;i++)
    {
        for(int k=0;k<6;k++)
        {
            meanweights[i][k]=6.*meanweights[i][k]/normweights[i]; //weight of channel k obtained relative to channel i
        }
   }

    cout << "final weights: " << endl;
    //take the mean for final weights
    for(int k=0;k<6;k++)
    {
        finalweights[k]=0;
        for(int i=0;i<6;i++)
        {
            finalweights[k]+=meanweights[i][k]/6.;
        }
    //print final weight
    cout << "weight channel " << k+1 << ": " << finalweights[k] << endl;
    }

    //save canvases in output file
    outfile->WriteTObject(c1);
    outfile->WriteTObject(c2);
    outfile->WriteTObject(c3);
    outfile->WriteTObject(c4);
    outfile->WriteTObject(c5);
    outfile->WriteTObject(c6);

    //close files
    outfile->Close();
    infile->Close();
}


