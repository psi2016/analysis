#include <iostream>
#include <sstream>

#include "TObject.h"
#include "TFile.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TVirtualPad.h"
#include "TLegend.h"

void cosmic_calibration()
{
    //initialise source tree
TFile* cosmicfile = new TFile("./../rootNew/cosmics/cosmics_sum_1494to1498.root");
TTree* events = (TTree*)cosmicfile->Get("events");
events->SetBranchStatus("*",0);
events->SetBranchStatus("gamma_ch1",1);
events->SetBranchStatus("gamma_ch2",1);
events->SetBranchStatus("gamma_ch3",1);
events->SetBranchStatus("gamma_ch4",1);
events->SetBranchStatus("gamma_ch5",1);
events->SetBranchStatus("gamma_ch6",1);
events->SetBranchStatus("gamma_sum_nocalib",1);
events->SetBranchStatus("time_gamma_sum_nocalib",1);

float ch1, ch2, ch3, ch4, ch5, ch6, time;
events->SetBranchAddress("gamma_ch1",&ch1);
events->SetBranchAddress("gamma_ch2",&ch2);
events->SetBranchAddress("gamma_ch3",&ch3);
events->SetBranchAddress("gamma_ch4",&ch4);
events->SetBranchAddress("gamma_ch5",&ch5);
events->SetBranchAddress("gamma_ch6",&ch6);
events->SetBranchAddress("time_gamma_sum_nocalib",&time);

    //create histograms to safe the data
TH1F* hist1 = new TH1F("hist1","hist1",1000,0, 100000);
TH1F* hist2 = new TH1F("hist2","hist2",1000,0, 100000);
TH1F* hist3 = new TH1F("hist3","hist3",1000,0, 100000);
TH1F* hist4 = new TH1F("hist4","hist4",1000,0, 100000);
TH1F* hist5 = new TH1F("hist5","hist5",1000,0, 100000);
TH1F* hist6 = new TH1F("hist6","hist6",1000,0, 100000);

    //Define cuts
const char* reccut = "(gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib >0.9 && (gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib < 1.1";


    //draw the channel and apply the time cut
//events->Draw("gamma_ch1>>hist1","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch1>10000","goff");
//events->Draw("gamma_ch2>>hist2","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch2>10000","goff");
//events->Draw("gamma_ch3>>hist3","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch3>10000","goff");
//events->Draw("gamma_ch4>>hist4","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch4>10000","goff");
//events->Draw("gamma_ch5>>hist5","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch5>10000","goff");
//events->Draw("gamma_ch6>>hist6","time_gamma_sum_nocalib>=142 && time_gamma_sum_nocalib<=146 && gamma_ch6>10000","goff");

events->Draw("gamma_ch1>>hist1",reccut,"goff");
events->Draw("gamma_ch2>>hist2",reccut,"goff");
events->Draw("gamma_ch3>>hist3",reccut,"goff");
events->Draw("gamma_ch4>>hist4",reccut,"goff");
events->Draw("gamma_ch5>>hist5",reccut,"goff");
events->Draw("gamma_ch6>>hist6",reccut,"goff");
    //match the peaks
Double_t peak1 = hist1->GetBinCenter(hist1->GetMaximumBin());
Double_t peak2 = hist2->GetBinCenter(hist2->GetMaximumBin());
Double_t peak3 = hist3->GetBinCenter(hist3->GetMaximumBin());
Double_t peak4 = hist4->GetBinCenter(hist4->GetMaximumBin());
Double_t peak5 = hist5->GetBinCenter(hist5->GetMaximumBin());
Double_t peak6 = hist6->GetBinCenter(hist6->GetMaximumBin());

std::cout << peak1 << " " << peak2 << " " << peak3 << " " << peak4 << " " << peak5 << " " << peak6 << std::endl;
Double_t mean = (peak1+peak2+peak3+peak4+peak5+peak6)/6;
Double_t f1 = mean/peak1;
Double_t f2 = mean/peak2;
Double_t f3 = mean/peak3;
Double_t f4 = mean/peak4;
Double_t f5 = mean/peak5;
Double_t f6 = mean/peak6;


    //create new calibrated histograms for graphical output
TH1F* calib_hist1 = new TH1F("calib_hist1","calib_hist1",100,0, 100000);
TH1F* calib_hist2 = new TH1F("calib_hist2","calib_hist2",100,0, 100000);
TH1F* calib_hist3 = new TH1F("calib_hist3","calib_hist3",100,0, 100000);
TH1F* calib_hist4 = new TH1F("calib_hist4","calib_hist4",100,0, 100000);
TH1F* calib_hist5 = new TH1F("calib_hist5","calib_hist5",100,0, 100000);
TH1F* calib_hist6 = new TH1F("calib_hist6","calib_hist6",100,0, 100000);
TH1F* calib_sum = new TH1F("calib_sum","calib_sum",100,0, 200000);

    //fill calibrated histograms
int nevents = events->GetEntries();
/*for(int j = 0; j<nevents; j++)
    {

    events->GetEntry(j);
    if(time < 142 || time >146){continue;}
    if(time>=142 && time <=146)
        {
        calib_hist1->Fill(f1*ch1);
        calib_hist2->Fill(f2*ch2);
        calib_hist3->Fill(f3*ch3);
        calib_hist4->Fill(f4*ch4);
        calib_hist5->Fill(f5*ch5);
        calib_hist6->Fill(f6*ch6);
        calib_sum->Fill(f1*ch1+f2*ch2+f3*ch3+f4*ch4+f5*ch5+f6*ch6);



        }
    }
*/
std::ostringstream cut1stream;
cut1stream << f1 << "*gamma_ch1>>calib_hist1";
const char* cut1char = cut1stream.str().c_str();
std::ostringstream cut2stream;
cut2stream << f2 << "*gamma_ch2>>calib_hist2";
const char* cut2char = cut2stream.str().c_str();
std::ostringstream cut3stream;
cut3stream << f3 << "*gamma_ch3>>calib_hist3";
const char* cut3char = cut3stream.str().c_str();
std::ostringstream cut4stream;
cut4stream << f4 << "*gamma_ch4>>calib_hist4";
const char* cut4char = cut4stream.str().c_str();
std::ostringstream cut5stream;
cut5stream << f5 << "*gamma_ch5>>calib_hist5";
const char* cut5char = cut5stream.str().c_str();
std::ostringstream cut6stream;
cut6stream << f6 << "*gamma_ch6>>calib_hist6";
const char* cut6char = cut6stream.str().c_str();
std::ostringstream sumstream;
sumstream << f1 << " * gamma_ch1 + " << f2 << " * gamma_ch2 + " << f3 << " * gamma_ch3 + " << f4 << " * gamma_ch4 + " << f5 << " * gamma_ch5 + " << f6 << " * gamma_ch6>>calib_sum";
const char* sumchar = sumstream.str().c_str();
events->Draw(cut1char,reccut,"goff");
events->Draw(cut2char,reccut,"goff");
events->Draw(cut3char,reccut,"goff");
events->Draw(cut4char,reccut,"goff");
events->Draw(cut5char,reccut,"goff");
events->Draw(cut6char,reccut,"goff");
events->Draw(sumchar,reccut,"goff");

    //draw calibrated histograms
TCanvas* calibrated_spectra = new TCanvas("calibrated_spectra","calibrated spectra");
calibrated_spectra->Divide(2,1);
calibrated_spectra -> cd(1);
calib_hist1->SetLineColor(kBlue);
calib_hist2->SetLineColor(kRed);
calib_hist3->SetLineColor(kBlack);
calib_hist4->SetLineColor(kGreen);
calib_hist5->SetLineColor(kOrange);
calib_hist6->SetLineColor(kViolet);

calib_hist1->Draw();
calib_hist2->Draw("SAME");
calib_hist3->Draw("SAME");
calib_hist4->Draw("SAME");
calib_hist5->Draw("SAME");
calib_hist6->Draw("SAME");

    //add legend
TLegend* leg = new TLegend(0.6,0.6,0.8,0.8);
leg->AddEntry(calib_hist1,"channel 1","l");
leg->AddEntry(calib_hist2,"channel 2","l");
leg->AddEntry(calib_hist3,"channel 3","l");
leg->AddEntry(calib_hist4,"channel 4","l");
leg->AddEntry(calib_hist5,"channel 5","l");
leg->AddEntry(calib_hist6,"channel 6","l");
leg->Draw("SAME");

    //compare calibrated and non calibrated sum
calibrated_spectra->cd(2);
calib_sum->SetLineColor(kRed);
calib_sum->Draw();
TH1F* sum_no_calib = new TH1F("sum_no_calib","sum_no_calib",100,0, 200000);
events->Draw("gamma_sum_nocalib>>sum_no_calib",reccut,"goff");
sum_no_calib->Draw("SAME");
    //add legend
TLegend* sumleg = new TLegend(0.6,0.6,0.8,0.8);
sumleg->AddEntry(calib_sum,"calibrated sum","l");
sumleg->AddEntry(sum_no_calib,"not calibrated sum","l");
sumleg->Draw("SAME");
calibrated_spectra->Update();
    //safe output
TFile* outfile = new TFile("cosmic_calibration.root","RECREATE");
outfile->cd();
outfile->WriteTObject(calibrated_spectra);
calibrated_spectra->Close();
outfile->Close();

std::cout << "factor1: " << f1 << "  factor2: " << f2 <<"  factor3: " << f3 <<"  factor4: " << f4 <<"  factor5: " << f5<<"  factor6: " << f6 << std::endl;
cosmicfile->Close();
}
