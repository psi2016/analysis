#include <iostream>
#include <sstream>

#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFractionFitter.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TH1.h"
#include "THStack.h"
#include "TLegend.h"
#include "TVirtualFitter.h"
#include "TCut.h"

void subtract_bkg_calibrated()
{
    //define fit range
    Double_t range_low  = 40E3;
    Double_t range_high = 70E3;

    // get signal and bkg files
    TFile* f_sig = new TFile("./../AnalysisRoot/calibration_scatter_sum_gamma_pi-stop.root");
    TFile* f_bkg = new TFile("./../AnalysisRoot/calibration_scatter_no_target_gamma_pi-stop_sum.root");

    //create output file
    TFile* f_out = new TFile ("./../AnalysisRoot/calibration_scatter_subtract_bkg_calibrated.root","RECREATE");

    // get histograms
    TH1D* h_sig = (TH1D*)f_sig->Get("h_cal");
    TH1D* h_bkg = (TH1D*)f_bkg->Get("h_cal");

    // get bin numbers for integration range
    Int_t bin_low = h_sig->FindBin(range_low);
    Int_t bin_high = h_sig->FindBin(range_high);

    //get scale factors
    Double_t norm_sig = h_sig->Integral(bin_low,bin_high);
    Double_t norm_bkg = h_bkg->Integral(bin_low,bin_high);

    Double_t scale = norm_sig/norm_bkg;

    std::cout << "scale" << scale << std::endl;
    std::cout << "norm_sig: " << norm_sig << "    norm_bkg: " << norm_bkg<< std::endl;

    //subtract background histograms
    TH1F* h_clean_sig = (TH1F*)h_sig->Clone("h_clean_signal");
    h_clean_sig->Add(h_bkg,-scale);

    TCanvas* c1 = new TCanvas("h_sig_AND_h_bkg","h_sig & h_bkg");
    TCanvas* c2 = new TCanvas("h_sig_MINUS_h_bkg","h_sig - h_bkg");

    // draw histograms
    c1->cd();
    h_bkg->Scale(scale);
    h_bkg->Draw("hist");
    h_bkg->SetFillColor(kRed);

    h_sig->Draw("same");

    c2->cd();
    h_clean_sig->Draw();


   //save output
   f_out->WriteTObject(h_sig);
   f_out->WriteTObject(h_clean_sig);
   f_out->WriteTObject(h_bkg);
   f_out->WriteTObject(c1);


   //close files and canvas
   //f_sig->Close();
   //f_bkg->Close();
   //f_out->Close();
}
