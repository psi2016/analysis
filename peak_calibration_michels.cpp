#include "TFile.h"
#include "TString.h"
#include <iostream>
#include "TTree.h"
#include "TCanvas.h"
#include <math.h>
#include "TH1.h"
#include <sstream>


void peak_calibration_michels()
{
// Author: Matthias Oscity

// INPUT VALUES - ADJUSTABLE - VALUES HERE HAVE IMPACT ON FITS & CUTS!

	string file;

	// -------------------
	
	// Runs 1245 to 1247: High Voltage Runs
	file = "michel_1245to1247.root";
	
	// Set Range for Gaus Fit
	Double_t xmin1 = 3200;	Double_t xmax1 = 25000; 
	Double_t xmin2 = 3500;	Double_t xmax2 = 25000; 
	Double_t xmin3 = 4000;	Double_t xmax3 = 25000; 
	Double_t xmin4 = 2800;	Double_t xmax4 = 25000; 
	Double_t xmin5 = 2500;	Double_t xmax5 = 25000; 
	Double_t xmin6 = 4000;	Double_t xmax6 = 25000;
	int summax = 200000; // Upper x-boundary for summed histograms (gamma_sum_nocalib and so on).
	
	// Number of Bins in all histograms
	int binning = 200; 
	
	// Cuts	
	int tgv_l = 140;	int tgv_h = 148;
	int tps_l = 142;	int tps_h = 152;
	int tgsn_l = 140;	int tgsn_h = 148;
	
	TCut cut1 = "140 <= time_gamma_veto && time_gamma_veto <= 148";
	TCut cut2 = "142 <= time_pi_stop_trig && time_pi_stop_trig <= 152";
	TCut cut3 = "140 <= time_gamma_sum_nocalib && time_gamma_sum_nocalib <= 148";
	
	
	// -------------------
	/*
	// Runs 470 to 474:
	
	file = "michel_470to474.root";
	Double_t xmin1 = 1500;	Double_t xmax1 = 11000;
	Double_t xmin2 = 1500;	Double_t xmax2 = 10000;
	Double_t xmin3 = 2000;	Double_t xmax3 = 13000;
	Double_t xmin4 = 1000;	Double_t xmax4 = 9000;
	Double_t xmin5 = 1500;	Double_t xmax5 = 12000;
	Double_t xmin6 = 1800;	Double_t xmax6 = 12000;
	int summax = 120000; // Upper x-boundary for summed histograms (gamma_sum_nocalib and so on).
	
	// Number of Bins in all histograms
	int binning = 100; 
	
	// Cuts
	int tgv_l = 142;	int tgv_h = 150;
	int tps_l = 144;	int tps_h = 152;
	int tgsn_l = 142;	int tgsn_h = 150;
	
	TCut cut1 = "142 <= time_gamma_veto && time_gamma_veto <= 150";
	TCut cut2 = "144 <= time_pi_stop_trig && time_pi_stop_trig <= 152";
	TCut cut3 = "142 <= time_gamma_sum_nocalib && time_gamma_sum_nocalib <= 150";
	*/
	// -------------------
	
	// General Cuts:
	TCut cut_n = " time_pi_stop_trig_n==1 ";
	TCut reco = " 0.8 <= (gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib && (gamma_ch1+gamma_ch2+gamma_ch3+gamma_ch4+gamma_ch5+gamma_ch6)/gamma_sum_nocalib <= 1.2";
	
	
	
// THE CODE - DON'T CHANGE THINGS FROM HERE ON UNLESS YOU KNOW WHAT YOU DO

// Variables Declaration


	std::ostringstream filestream;
	std::ostringstream filepathstream;
	
	filepathstream << "./../rootNew/michel/" << file; 		// File Path
	const char *filepath = filepathstream.str().c_str();
	
	const char* filename = file.c_str();
	
	float val1;
	float val2;
	float val3;
	float val4;
	float val5;
	float val6;
	float calosum;
	float energynocalib;
	float initialenergy;
	
	float out1 = 0;
	float out2 = 0;
	float out3 = 0;
	float out4 = 0;
	float out5 = 0;
	float out6 = 0;
	float out7 = 0;
	
	float time_gamma_sum_nocalib; 					
	vector<int> *time_gamma_veto_pointer = 0;		
	vector<int> *time_pi_stop_pointer = 0;			
	int time_gamma_veto = -1;
	int time_pi_stop = -1;
	int time_gamma_veto_value = 0;
	int time_pi_stop_value = 0;
	int gamma_veto_size = 0;
	int pi_stop_size = 0;
	
// Open File and read Tree
	
	TFile* f_input = new TFile(filepath, filename);
	TTree* tree = (TTree*)f_input->Get("events");
	
// Create Output File

	TFile* f_output = new TFile("michels_shifted.root", "recreate");

// Create new Trees & Branches to save in "tree" format
	
	TTree* t_out1 = new TTree("t_gamma_sum_manual", "Out1");
	TTree* t_out2 = new TTree("t_gamma_sum_manual_reco_cut", "Out2");
	TTree* t_out3 = new TTree("t_gamma_sum_manual_all_cuts", "Out3");
	TTree* t_out4 = new TTree("t_gamma_sum_nocalib", "Out4");
	TTree* t_out5 = new TTree("t_gamma_sum_nocalib_c", "Out5");
	TTree* t_out6 = new TTree("t_gamma_sum_manual_r_nocut", "Out6");
	TTree* t_out7 = new TTree("t_gamma_sum_manual_r", "Out7");
	
	t_out1->Branch("t_gamma_sum_manual", &out1);
	t_out2->Branch("t_gamma_sum_manual_reco_cut", &out2);
	t_out3->Branch("t_gamma_sum_manual_all_cuts", &out3);
	t_out4->Branch("t_gamma_sum_nocalib", &out4);
	t_out5->Branch("t_gamma_sum_nocalib_c", &out5);
	t_out6->Branch("t_gamma_sum_manual_r_nocut", &out6);
	t_out7->Branch("t_gamma_sum_manual_r", &out7);
	

// Disable / Enable Branches
	
	tree->SetBranchStatus("*",0);
	tree->SetBranchStatus("gamma_ch1",1);
	tree->SetBranchStatus("gamma_ch2",1);
	tree->SetBranchStatus("gamma_ch3",1);
	tree->SetBranchStatus("gamma_ch4",1);
	tree->SetBranchStatus("gamma_ch5",1);
	tree->SetBranchStatus("gamma_ch6",1);
	tree->SetBranchStatus("gamma_sum_nocalib",1);
	tree->SetBranchStatus("time_gamma_sum_nocalib",1);
	tree->SetBranchStatus("time_gamma_veto",1);
	tree->SetBranchStatus("time_pi_stop_trig",1);
	
// Set Branch Addresses
	
	tree->SetBranchAddress("gamma_ch1",&val1);
	tree->SetBranchAddress("gamma_ch2",&val2);
	tree->SetBranchAddress("gamma_ch3",&val3);
	tree->SetBranchAddress("gamma_ch4",&val4);
	tree->SetBranchAddress("gamma_ch5",&val5);
	tree->SetBranchAddress("gamma_ch6",&val6);
	tree->SetBranchAddress("gamma_sum_nocalib",&energynocalib);
	tree->SetBranchAddress("time_gamma_sum_nocalib",&time_gamma_sum_nocalib);
	tree->SetBranchAddress("time_gamma_veto",&time_gamma_veto_pointer);		// This one needs a integer pointer
	tree->SetBranchAddress("time_pi_stop_trig",&time_pi_stop_pointer);		// And this too
	
// Get upper x-boundaries

	Double_t max1 = tree->GetMaximum("gamma_ch1");
	Double_t max2 = tree->GetMaximum("gamma_ch2");
	Double_t max3 = tree->GetMaximum("gamma_ch3");
	Double_t max4 = tree->GetMaximum("gamma_ch4");
	Double_t max5 = tree->GetMaximum("gamma_ch5");
	Double_t max6 = tree->GetMaximum("gamma_ch6");
	//Double_t max7 = tree->GetMaximum("gamma_sum_nocalib"); 
	
// Create initial histogram

	TH1F* h1 = new TH1F("h_gamma_ch1","gamma_ch1",binning,0,max1); 
	TH1F* h2 = new TH1F("h_gamma_ch2","gamma_ch2",binning,0,max2); 
	TH1F* h3 = new TH1F("h_gamma_ch3","gamma_ch3",binning,0,max3); 
	TH1F* h4 = new TH1F("h_gamma_ch4","gamma_ch4",binning,0,max4); 
	TH1F* h5 = new TH1F("h_gamma_ch5","gamma_ch5",binning,0,max5); 
	TH1F* h6 = new TH1F("h_gamma_ch6","gamma_ch6",binning,0,max6);
	TH1F* h_sum_nocalib = new TH1F("h_gamma_sum_nocalib","gamma_sum_nocalib RAW",binning,0,summax);
	TH1F* h_sum_manual = new TH1F("h_gamma_sum_manual","gamma_sum_manual (Sum of ch1-ch6) with no cuts",binning,0,summax); 
	TH1F* h_sum_manual_recocut = new TH1F("h_gamma_sum_manual_reco_cut","gamma_sum_manual_cut with reco cut",binning,0,summax); 
	
// Create histograms with cuts

	TH1F* h1_c = new TH1F("h_gamma_ch1_c","gamma_ch1 with all Cuts",binning,0,max1); 
	TH1F* h2_c = new TH1F("h_gamma_ch2_c","gamma_ch2 with all Cuts",binning,0,max2); 
	TH1F* h3_c = new TH1F("h_gamma_ch3_c","gamma_ch3 with all Cuts",binning,0,max3); 
	TH1F* h4_c = new TH1F("h_gamma_ch4_c","gamma_ch4 with all Cuts",binning,0,max4); 
	TH1F* h5_c = new TH1F("h_gamma_ch5_c","gamma_ch5 with all Cuts",binning,0,max5); 
	TH1F* h6_c = new TH1F("h_gamma_ch6_c","gamma_ch6 with all Cuts",binning,0,max6); 
	TH1F* h_sum_nocalib_c = new TH1F("h_gamma_sum_nocalib_c","gamma_sum_nocalib with all cuts",binning,0,summax);
	TH1F* h_sum_manual_cuts = new TH1F("h_gamma_sum_manual_all_cuts","gamma_sum_manual_cut with all cuts",binning,0,summax); 

	
// Fill Histograms
	
	int nentries = tree->GetEntries();
	
	for(int j = 0; j < nentries; j++){
		tree->GetEntry(j); 	// Reads the x-Values of the entry j and writes them to where specified above with SetBranchAddress (val1, val2, ...)
		//std::cout << "Entry = " << j << "   GetEntry(j) = " << tree->GetEntry(j) << "     val1 = " << val1 << std::endl;
		
		initialenergy= val1+val2+val3+val4+val5+val6;
		// std:::cout << "Difference = " << TMath::Abs(initialenergy - energynocalib) << std::endl;  // is always not zero.
		
		// Values for new tree
		out1 = initialenergy;
		out4 = energynocalib;
		
		// Fill new tree
		t_out1->Fill();
		t_out4->Fill();
		
		// Fill Histograms
		h_sum_nocalib->Fill(energynocalib);
		h_sum_manual->Fill(initialenergy);
		
		// Fill Histograms with reco cut
		if(0.90 < initialenergy/energynocalib && initialenergy/energynocalib < 1.1){
			h_sum_manual_recocut->Fill(initialenergy);	// Histogram "Sum Manual Cut" has the direct sum of val1 to val6 with 
			h1->Fill(val1);								// the cut 0.9 < sum_of_values/gamma_sum_nocalib_value < 1.1
			h2->Fill(val2);
			h3->Fill(val3);
			h4->Fill(val4);
			h5->Fill(val5);
			h6->Fill(val6);
			out2 = initialenergy;
			t_out2->Fill();
		}
		
		
		// Reset time variables to -1 <=> "FALSE".
		time_gamma_veto = -1;
		time_pi_stop = -1;
		
		// Reset also values to 0... but not really necessary.
		time_gamma_veto_value = 0;
		time_pi_stop_value = 0;
		
		// Check if there is an time value in "time_gamma_veto_pointer" which lies in the time window [tgv_l, tgv_h]
		// If there is one, then time_gamma_veto != -1 <=> "TRUE"
		
		gamma_veto_size = time_gamma_veto_pointer->size(); 		// size is mostly 5+
		for(int i = 0; i < gamma_veto_size; i++){
			time_gamma_veto_value = (*time_gamma_veto_pointer)[i];
			if( tgv_l <= time_gamma_veto_value && tgv_h >= time_gamma_veto_value){
				time_gamma_veto = i; // sets it to 0,1,2... if there is an entry in time_gamma_veto_pointer which lies between tgv_l and tgv_h
				break;				// Stops for-loop if there is any entry which matches.
			}
		}
		
		// Same for pi_stop
		pi_stop_size = time_pi_stop_pointer->size(); 			// size is mostly 1+
		for(int i = 0; i < pi_stop_size; i++){
			time_pi_stop_value = (*time_pi_stop_pointer)[i];
			if( tps_l <= time_pi_stop_value && tps_h >= time_pi_stop_value){
				time_pi_stop = i;
				break;
			}
		}
		
		// Fill Histograms with all cuts  
		if( time_gamma_veto != -1  && time_pi_stop != -1  && tgsn_l <= time_gamma_sum_nocalib && tgsn_h >= time_gamma_sum_nocalib && 0.90 < initialenergy/energynocalib && initialenergy/energynocalib < 1.1){
			h1_c->Fill(val1);
			h2_c->Fill(val2);
			h3_c->Fill(val3);
			h4_c->Fill(val4);
			h5_c->Fill(val5);
			h6_c->Fill(val6);
			h_sum_nocalib_c->Fill(energynocalib);
			h_sum_manual_cuts->Fill(initialenergy);
			out3 = initialenergy;
			out5 = energynocalib;
			t_out3->Fill();
			t_out5->Fill();
		}
		else{continue;}
	}
	
// Perform Fit

	h1->Fit("gaus","Q","",xmin1,xmax1); // Option "Q" is for "quiet" = Don't print values in terminal
	h2->Fit("gaus","Q","",xmin2,xmax2); // Option "0" is for "dont plot the result of the fit." -> Results in that no fit is drawn at all.
	h3->Fit("gaus","Q","",xmin3,xmax3); // Combine options with "Q 0" and so on.
	h4->Fit("gaus","Q","",xmin4,xmax4);
	h5->Fit("gaus","Q","",xmin5,xmax5);
	h6->Fit("gaus","Q","",xmin6,xmax6);
	
// Get Fit Parameters back

	TF1* fit1 = h1->GetFunction("gaus");
	TF1* fit2 = h2->GetFunction("gaus");
	TF1* fit3 = h3->GetFunction("gaus");
	TF1* fit4 = h4->GetFunction("gaus");
	TF1* fit5 = h5->GetFunction("gaus");
	TF1* fit6 = h6->GetFunction("gaus");
	
	Double_t chi2_1 = fit1->GetChisquare();
	Double_t dof_1 = fit1->GetNDF();
	Double_t prob_1 = fit1->GetProb();
	Double_t mean_1 = fit1->GetParameter(1); // Parameter 0 = Constant, 1 = Mean, 2 = Sigma
	Double_t sigma_1 = fit1->GetParameter(2);
	
	Double_t chi2_2 = fit2->GetChisquare();
	Double_t dof_2 = fit2->GetNDF();
	Double_t prob_2 = fit2->GetProb();
	Double_t mean_2 = fit2->GetParameter(1);
	Double_t sigma_2 = fit2->GetParameter(2);
	
	Double_t chi2_3 = fit3->GetChisquare();
	Double_t dof_3 = fit3->GetNDF();
	Double_t prob_3 = fit3->GetProb();
	Double_t mean_3 = fit3->GetParameter(1);
	Double_t sigma_3 = fit3->GetParameter(2);

	Double_t chi2_4 = fit4->GetChisquare();
	Double_t dof_4 = fit4->GetNDF();
	Double_t prob_4 = fit4->GetProb();
	Double_t mean_4 = fit4->GetParameter(1);
	Double_t sigma_4 = fit4->GetParameter(2);
	
	Double_t chi2_5 = fit5->GetChisquare();
	Double_t dof_5 = fit5->GetNDF();
	Double_t prob_5 = fit5->GetProb();
	Double_t mean_5 = fit5->GetParameter(1);
	Double_t sigma_5 = fit5->GetParameter(2);
	
	Double_t chi2_6 = fit6->GetChisquare();
	Double_t dof_6 = fit6->GetNDF();
	Double_t prob_6 = fit6->GetProb();
	Double_t mean_6 = fit6->GetParameter(1);
	Double_t sigma_6 = fit6->GetParameter(2);
	
// Get Errors
	
	Double_t mean_1_err = fit1->GetParError(1);
	Double_t sigma_1_err = fit1->GetParError(2);
	
	Double_t mean_2_err = fit2->GetParError(1);
	Double_t sigma_2_err = fit2->GetParError(2);
	
	Double_t mean_3_err = fit3->GetParError(1);
	Double_t sigma_3_err = fit3->GetParError(2);
	
	Double_t mean_4_err = fit4->GetParError(1);
	Double_t sigma_4_err = fit4->GetParError(2);
	
	Double_t mean_5_err = fit5->GetParError(1);
	Double_t sigma_5_err = fit5->GetParError(2);
	
	Double_t mean_6_err = fit6->GetParError(1);
	Double_t sigma_6_err = fit6->GetParError(2);
	

// Method: Take gamma_ch1 values as reference

	// Set Reference Peak and Sigma
	Double_t mean_ref = mean_1;
	Double_t sig_ref = sigma_1;
	
	// Update: dont take peaks but point at half hight on right side.
	Double_t HH_ref = mean_1 + sigma_1*sqrt(2*log(2));
	Double_t HH_2 = mean_2 + sigma_2*sqrt(2*log(2));
	Double_t HH_3 = mean_3 + sigma_3*sqrt(2*log(2));
	Double_t HH_4 = mean_4 + sigma_4*sqrt(2*log(2));
	Double_t HH_5 = mean_5 + sigma_5*sqrt(2*log(2));
	Double_t HH_6 = mean_6 + sigma_6*sqrt(2*log(2));
	
	// Take maximum of upper boundary x
	Double_t newmax = TMath::Max( TMath::Max( TMath::Max( TMath::Max( TMath::Max(max1,max2), max3), max4), max5), max6);
	
	// Determine HH offsets
	
	Double_t Offset12 = HH_ref - HH_2;
	Double_t Offset13 = HH_ref - HH_3;
	Double_t Offset14 = HH_ref - HH_4;
	Double_t Offset15 = HH_ref - HH_5;
	Double_t Offset16 = HH_ref - HH_6;
	
	// Determine Rescale Factor with sigmas
	
	Double_t Stretch2 = sig_ref / sigma_2;
	Double_t Stretch3 = sig_ref / sigma_3;
	Double_t Stretch4 = sig_ref / sigma_4;
	Double_t Stretch5 = sig_ref / sigma_5;
	Double_t Stretch6 = sig_ref / sigma_6;

	// Create new Histograms

	TH1F* h1_r = new TH1F("h_gamma_ch1_r","Rescaled gamma_ch1",binning,0,newmax); 
	TH1F* h2_r = new TH1F("h_gamma_ch2_r","Rescaled gamma_ch2",binning,0,newmax); 
	TH1F* h3_r = new TH1F("h_gamma_ch3_r","Rescaled gamma_ch3",binning,0,newmax); 
	TH1F* h4_r = new TH1F("h_gamma_ch4_r","Rescaled gamma_ch4",binning,0,newmax); 
	TH1F* h5_r = new TH1F("h_gamma_ch5_r","Rescaled gamma_ch5",binning,0,newmax); 
	TH1F* h6_r = new TH1F("h_gamma_ch6_r","Rescaled gamma_ch6",binning,0,newmax);
	TH1F* h_sum_manual_r_nocut = new TH1F("h_gamma_sum_manual_r_nocut","Rescaled gamma_sum_manual w/o Cuts",binning,0,summax);
	TH1F* h_sum_manual_r = new TH1F("h_gamma_sum_manual_r","Rescaled gamma_sum_manual",binning,0,summax);
	
	// Draw Rescaled Histograms

	for(int j = 0; j < nentries; j++){
		tree->GetEntry(j);		
	
		initialenergy= val1+val2+val3+val4+val5+val6;
		
		// Resets
		time_gamma_veto = -1;
		time_pi_stop = -1;
		time_gamma_veto_value = 0;
		time_pi_stop_value = 0;
		
		// Check gamma veto time
		gamma_veto_size = time_gamma_veto_pointer->size();
		for(int i = 0; i < gamma_veto_size; i++){
			time_gamma_veto_value = (*time_gamma_veto_pointer)[i];
			if( tgv_l <= time_gamma_veto_value && tgv_h >= time_gamma_veto_value){
				time_gamma_veto = i;
				break;
			}
		}
		// Same for pi_stop
		pi_stop_size = time_pi_stop_pointer->size(); // size is 1+
		for(int i = 0; i < pi_stop_size; i++){
			time_pi_stop_value = (*time_pi_stop_pointer)[i];
			if( tps_l <= time_pi_stop_value && tps_h >= time_pi_stop_value){
				time_pi_stop = i;
				break;
			}
		}
		
		/*	How the following filling works step by step:
		temp_val2 =  val2 + Offset12; // New shifted Values
		new_HH_2 = HH_2 + Offset12; // New shifted HH
		new_HH_2 = HH_ref
		temp_val3 = temp_val2 - HH_ref; // Distance from new Value to new HH
		temp_val3 = val2 - HH_2
		temp_val4 = temp_val3 * Stretch2	//Stretch distance (new Mean remains unchanged)
		temp_val_final = temp_val4 + HH_ref // Add to new_HH
		
		// Then write temp_val_final into histogram, which is
		temp_val_final = HH_ref + Stretch2 * (val2 - HH_2)
		*/
		
		out6 = val1 + 5*HH_ref + Stretch2 *(val2 - HH_2) + Stretch3 * (val3 - HH_3)  + Stretch4 * (val4 - HH_4) + Stretch5 * (val5 - HH_5) + Stretch6 * (val6 - HH_6);
		t_out6->Fill();
		h_sum_manual_r_nocut->Fill(out6);
		
		if(0.90 < initialenergy/energynocalib && initialenergy/energynocalib < 1.1 && time_gamma_veto != -1  && time_pi_stop != -1  && tgsn_l <= time_gamma_sum_nocalib && tgsn_h >= time_gamma_sum_nocalib){
			h1_r->Fill(val1);
			h2_r->Fill(HH_ref + Stretch2 * (val2 - HH_2) ); 
			h3_r->Fill(HH_ref + Stretch3 * (val3 - HH_3) );			
			h4_r->Fill(HH_ref + Stretch4 * (val4 - HH_4) );			
			h5_r->Fill(HH_ref + Stretch5 * (val5 - HH_5) );
			h6_r->Fill(HH_ref + Stretch6 * (val6 - HH_6) );
			
			out7 = val1 + 5*HH_ref + Stretch2 * (val2 - HH_2) + Stretch3 * (val3 - HH_3) + Stretch4 * (val4 - HH_4)  + Stretch5 * (val5 - HH_5) + Stretch6 * (val6 - HH_6);
			h_sum_manual_r->Fill( out7 );
			t_out7->Fill();
		}
	}
	
	
// Print Parameters

	std::cout << "Gaus Fits			" 		<< "ch1" 							<< "		" << 	"ch2" 						<< "		" << "ch3" 							<< "		" << "ch4" 							<< "		" << "ch5" 							<< "		" << "ch6" 							<< std::endl;
	std::cout << "Mean				" 		<< Form("%.1f",mean_1 )				<< "		" << Form("%.1f",mean_2) 			<< "		" << Form("%.1f",mean_3) 			<< "		" << Form("%.1f",mean_4) 			<< "		" << Form("%.1f",mean_5) 			<< "		" << Form("%.1f",mean_6) 			<< std::endl;
	std::cout << "Old Half Height Right		" 		<< Form("%.1f",HH_ref )				<< "		" << Form("%.1f",HH_2) 			<< "		" << Form("%.1f",HH_3) 			<< "		" << Form("%.1f",HH_4) 			<< "		" << Form("%.1f",HH_5) 			<< "		" << Form("%.1f",HH_6) 			<< std::endl;
	std::cout << "New Mean 			" 		<< Form("%.1f",mean_1)				<< "		" << Form("%.1f",HH_ref + Stretch2 * (mean_2- HH_2)) 			<< "		" << Form("%.1f",HH_ref + Stretch3 * (mean_3- HH_3)) 			<< "		" << Form("%.1f",HH_ref + Stretch4 * (mean_4- HH_4)) 			<< "		" << Form("%.1f",HH_ref + Stretch5 * (mean_5- HH_5)) 			<< "		" << Form("%.1f",HH_ref + Stretch6 * (mean_6- HH_6)) 			<< std::endl;
	std::cout << "Sigma				" 		<< Form("%.1f",sigma_1 )			<< "		" << Form("%.1f",sigma_2) 			<< "		" << Form("%.1f",sigma_3) 			<< "		" << Form("%.1f",sigma_4) 			<< "		" << Form("%.1f",sigma_5) 			<< "		" << Form("%.1f",sigma_6) 			<< std::endl;
	std::cout << "Sigma/Mean			" 	<< Form("%.3f",sigma_1/mean_1) 		<< "		" << Form("%.3f",sigma_2/mean_2)	<< "		" << Form("%.3f",sigma_3/mean_3) 	<< "		" << Form("%.3f",sigma_4/mean_4)	<< "		" << Form("%.3f",sigma_5/mean_5) 	<< "		" << Form("%.3f",sigma_6/mean_6) 	<< std::endl;
	std::cout << "Offsets				" 	<< "0"  							<< "		" << Form("%.3f",Offset12)			<< "	" 	  << Form("%.3f",Offset13)			<< "	"     << Form("%.3f",Offset14)			<< "	"     << Form("%.3f",Offset15)			<< "	"     << Form("%.3f",Offset16)			<< std::endl;
	std::cout << "Sig_i/Sig_1			" 	<< "1"  							<< "		" << Form("%.3f",Stretch2)			<< "		" << Form("%.3f",Stretch3)			<< "		" << Form("%.3f",Stretch4)			<< "		" << Form("%.3f",Stretch5)			<< "		" << Form("%.3f",Stretch6)			<< std::endl;
	std::cout << "Chi2				" 		<< Form("%.3f",chi2_1 )				<< "		" << Form("%.3f",chi2_2) 			<< "		" << Form("%.3f",chi2_3) 			<< "		" << Form("%.3f",chi2_4) 			<< "		" << Form("%.3f",chi2_5) 			<< "		" << Form("%.3f",chi2_6)			<< std::endl; // Should be low
	std::cout << "DoF				" 		<< dof_1							<< "		" << dof_2 							<< "		" << dof_3 							<< "		" << dof_4 							<< "		" << dof_5 							<< "		" << dof_6 							<< std::endl;
	std::cout << "Chi2/DoF			" 		<< Form("%.3f",chi2_1/dof_1) 		<< "		" << Form("%.3f",chi2_2/dof_2)		<< "		" << Form("%.3f",chi2_3/dof_3) 		<< "		" << Form("%.3f",chi2_4/dof_4) 		<< "		" << Form("%.3f",chi2_5/dof_5) 		<< "		" << Form("%.3f",chi2_6/dof_6) 		<< std::endl;
	std::cout << "Prob[10^-6]			" 	<< Form("%.3f",prob_1 *10**6)		<< "		" << Form("%.3f",prob_2* 10**6) 	<< "		" << Form("%.3f",prob_3 * 10**6) 	<< "		" << Form("%.3f",prob_4 * 10**6) 	<< "		" << Form("%.3f",prob_5 * 10**6) 	<< "	"     << Form("%.3f",prob_6*10**6)		<< std::endl; // Should be high


// Activate all branches again
	
	tree->SetBranchStatus("*",1);

// Save & Write
 	
	f_output->Write();
	
// Close or Delete

	f_input->Close();
	f_output->Close();
	
	f_input->Delete();
	f_output->Delete();
	
// Print "Done"

	printf("%s\n","Done. See file michels_shifted.root");
}
